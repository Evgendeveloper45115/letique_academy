<?php

namespace app\models;

use phpDocumentor\Reflection\Types\Self_;
use Yii;
use yii\helpers\VarDumper;

/**
 * This is the model class for table "brand".
 *
 * @property int $id
 * @property string $cat
 * @property string $name
 * @property string $video
 * @property string $image
 * @property string $description
 * @property string $date_create
 * @property string $date_to
 * @property string $location
 * @property ContentHasImage[] $images
 * @property Menu $menu
 */
class Content extends \yii\db\ActiveRecord
{
    public $img;
    public $img2;

    public static $design_ids = [
        1 => '#1 (статистики курсов)',
        2 => '#2 (обуч.курсе)',
        3 => '#3 (Текущие активности)',
        4 => '#4 (Материалы)',
        5 => '#5 (Создание клиентов)',
        6 => '#6 (Лента новостей)',
        7 => '#7 Тестирование',
    ];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'content';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cat', 'name', 'video', 'image', 'description', 'date_create', 'date_to', 'location'], 'safe'],
            [['description'], 'string'],
            [['cat', 'name', 'video', 'image'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'cat' => 'Категория',
            'name' => 'Название',
            'video' => 'Ссылка Ютуб',
            'image' => 'Превью',
            'description' => 'Описание',
            'location' => 'Локация',
            'date_create' => 'Дата создания',
            'date_to' => 'До какой даты',
        ];
    }

    public function getImages()
    {
        return $this->hasMany(ContentHasImage::class, ['content_id' => 'id']);
    }

    public function getMenu()
    {
        return $this->hasOne(Menu::class, ['id' => 'cat']);
    }

    public function getMenuNews()
    {
        return $this->hasOne(Menu::class, ['id' => 'cat'])->andWhere(['is_news' => true]);
    }


    public function getImagesNames()
    {
        $obj = $this->hasMany(ContentHasImage::class, ['content_id' => 'id'])->all();
        $links = [];
        $links_id = [];
        $dir = '/uploads/' . $_GET['category'] . '/';
        foreach ($obj as $image) {
            $links[] = $dir . $image->name;
            $links_id[]['key'] = $image->id;
        }
        return ['link' => $links, 'id' => $links_id];
    }

    public function getImagesNamesAvatar()
    {
        $links = [];
        $links_id = [];
        $dir = '/uploads/' . $_GET['category'] . '/';
        $links[] = $dir . $this->image;
        $links_id[]['key'] = $this->id;
        if ($this->image) {
            return ['link' => $links, 'id' => $links_id];
        }
        return ['link' => $links, 'id' => $links_id];
    }

    public static function contentSearch()
    {
        $url = \Yii::$app->request->url;
        $cat = end(explode('/', $url));
        if (stristr($cat, 'Seacrch')) {
            $cat = explode('?', $cat)[0];
        }
        $menu = Menu::findOne(['url' => $cat]);
        return Content::find()->where(['cat' => $menu->id]);
    }

    public static function menuSearch()
    {
        $url = \Yii::$app->request->url;
        $cat = end(explode('/', $url));
        if (stristr($cat, 'Seacrch')) {
            $cat = explode('?', $cat)[0];
        }
        if (is_numeric($cat)) {
            return Content::findOne($cat)->menu;
        }
        return Menu::findOne(['url' => $cat]);
    }

    public static function getRender($design_id)
    {

        switch ($design_id) {
            case 1 :
                $render = 'statistic_page';
                break;
            case 2 :
                $render = 'obucausij-kurs';
                break;
            case 3 :
                $render = 'tekusie-aktivnosti';
                break;
            case 4 :
                $render = 'materialy';
                break;
            case 5 :
                $render = 'users';
                break;
            case 6 :
                $render = 'lenta-novostej';
                break;
            case 7 :
                $render = 'tests';
                break;
            default:
                $render = 'no-content';
        }
        if ($render == 'tests' && isset($_GET['sub_category'])) {
            $menu = Menu::findOne(['url' => $_GET['sub_category']]);
            if ($menu->design_id != 7) {
                return 'tests-2';
            }
        }
        return $render;
    }

    public static function getMonths($date)
    {
        $arr = [
            1 => 'января',
            2 => 'февраля',
            3 => 'марта',
            4 => 'апреля',
            5 => 'мая',
            6 => 'июня',
            7 => 'июля',
            8 => 'августа',
            9 => 'сентября',
            10 => 'октября',
            11 => 'ноября',
            12 => 'декабря'
        ];

// Поскольку от 1 до 12, а в массиве, как мы знаем, отсчет идет от нуля (0 до 11),
// то вычитаем 1 чтоб правильно выбрать уже из нашего массива.

        return $arr[date('n')];
    }

    public function getNext()
    {
        return $this->find()->where(['>', 'id', $this->id])->andWhere(['cat' => $_GET['category']])->one();
    }

    public function getPrev()
    {
        return $this->find()->where(['<', 'id', $this->id])->andWhere(['cat' => $_GET['category']])->orderBy('id desc')->one();
    }

}
