<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "content_has_image".
 *
 * @property int $id
 * @property int|null $content_id
 * @property string|null $name
 * @property string|null $title
 */
class ContentHasImage extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'content_has_image';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['content_id'], 'integer'],
            [['name', 'title'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'content_id' => 'Content ID',
            'name' => 'Name',
        ];
    }
}
