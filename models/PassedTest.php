<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "passedtest".
 *
 * @property int $id
 * @property int|null $user_id
 * @property int|null $test_id
 * @property string|null $percent
 * @property string|null $answers
 * @property int $try
 * @property string|null $time_left
 */
class PassedTest extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'passedtest';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'test_id', 'try'], 'integer'],
            [['percent', 'answers', 'time_left'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'test_id' => 'Test ID',
            'percent' => 'Percent',
            'answers' => 'Answers',
            'try' => 'Try',
            'time_left' => 'Time Left',
        ];
    }
}
