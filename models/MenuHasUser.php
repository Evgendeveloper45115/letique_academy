<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "menu_has_user".
 *
 * @property int $id
 * @property int $role
 * @property int $menu_id
 */
class MenuHasUser extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'menu_has_user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['role', 'menu_id'], 'required'],
            [['role', 'menu_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'role' => 'Role',
            'menu_id' => 'Menu ID',
        ];
    }
}
