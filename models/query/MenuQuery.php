<?php

namespace app\models\query;

use creocoder\nestedsets\NestedSetsQueryBehavior;
use paulzi\nestedsets\NestedSetsQueryTrait;

class MenuQuery extends \yii\db\ActiveQuery
{
    public function behaviors()
    {
        return [
            NestedSetsQueryBehavior::class,
        ];
    }
}