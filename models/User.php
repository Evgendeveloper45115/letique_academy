<?php

namespace app\models;

use app\components\MyUrlManager;
use app\helpers\My;
use app\models\base\MyAR;
use Yii;
use yii\helpers\VarDumper;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string $email
 * @property string $password
 * @property string $first_name
 * @property string $last_name
 * @property string $date_registration
 * @property integer $role
 * @property string $phone
 * @property string $city
 * @property string $avatar
 * @property integer $franchise_id
 * @property User[] $sellers
 */
class User extends MyAR implements IdentityInterface
{
    public $img;
    public $orig_password;
    public $new_password;
    public $password1;
    public $email_confirm;

    const ROLE_USER = 0;
    const ROLE_WHOLESALER = 4;
    const ROLE_FRANCHISE = 5;
    const ROLE_SELLER = 6;
    const ROLE_CURATOR = 1;
    const ROLE_ADMIN = 2;
    const ROLE_PUBLISHER = 3;
    const SCENARIO_CHANGE_AVATAR = 'change_user_avatar';
    const SCENARIO_CHANGE_PASSWORD_USER = 'change_password_user';
    const SCENARIO_USER_CREATE = 'user_create';
    const SCENARIO_USER_CREATE_COUPLE = 'user_create_couple';
    const CREATE_USER = 'create_user';


    static $roles = [
        self::ROLE_WHOLESALER => 'Оптовик',
        self::ROLE_FRANCHISE => 'Франчайз',
        self::ROLE_SELLER => 'Продавец',
    ];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user';
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_CHANGE_AVATAR] = ['email'];
        $scenarios[self::SCENARIO_CHANGE_PASSWORD_USER] = ['orig_password', 'new_password', 'password1',];
        $scenarios[self::SCENARIO_USER_CREATE] = ['email', 'email_confirm', 'email_man', 'point'];
        $scenarios[self::SCENARIO_USER_CREATE_COUPLE] = ['email', 'email_confirm', 'email_man', 'point'];
        $scenarios[self::CREATE_USER] = ['email', 'first_name', 'last_name', 'password', 'phone', 'city'];
        return $scenarios;
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['email', 'orig_password', 'new_password', 'password1', 'first_name', 'last_name', 'password'], 'required'],
            ['avatar', 'required', 'message' => "Загрузите фото"],
            [['email'], 'unique'],
            [['email'], 'email'],
            [['date_registration', 'password', 'avatar', 'city', 'phone', 'franchise_id', 'role'], 'safe'],
            [['email', 'password', 'first_name', 'last_name'], 'string', 'max' => 255],
            ['password1', 'compare',
                'compareAttribute' => 'new_password',
                'on' => self::SCENARIO_CHANGE_PASSWORD_USER,
                'message' => 'Пароли не совпадают'
            ],
            ['orig_password', 'myCompare', 'on' => self::SCENARIO_CHANGE_PASSWORD_USER],
        ];
    }

    public function myCompare($attribute, $params)
    {
        if (!password_verify($this->$attribute, $this->password) && $this->$attribute != $this->password) {
            $this->addError($attribute, 'Старый пароль не совпадает');
        }
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'email' => 'Email',
            'date_registration' => 'Дата регистрации',
            'first_name' => 'Имя',
            'last_name' => 'Фамилия',
            'patronymic' => 'Отчество',
            'role' => 'Role',
            'point' => 'Баллы',
        ];
    }


    /**
     * @param int|string $id
     * @return User|IdentityInterface|null
     */
    public static function findIdentity($id)
    {
        $session = Yii::$app->session;
        if ($session->get('avatar')) {
            $id = $session->get('avatar');
        }
        return static::findOne($id);
    }

    public function getFIO()
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    /**
     * @param $token
     * @param null $type
     * @return |null
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return null;
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getAuthKey()
    {
        return $this->authKey;

    }


    /**
     * @param $authKey
     * @return bool
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }


    public function sendMail($password)
    {

        /* @var \app\models\User $user */
        $user = Yii::$app->getUser()->identity;
        $text = 'registration-html-' . My::$type_program[$user->session_type];
        $subject = Yii::$app->params['email-subject-registration-' . My::$type_program[$user->session_type]];

        My::sendEmail($this->groupMember, $text, $subject, $password);
    }


    public function login()
    {
        return Yii::$app->user->login($this);
    }


    public function getSellers()
    {
        return $this->hasMany(User::class, ['franchise_id' => 'id'])->orderBy($_GET['sort'] . ' ' . $_GET['sort_type']);
    }
}
