<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "testQuestion".
 *
 * @property int $id
 * @property string $test_one
 * @property string $test_two
 * @property string $test_three
 * @property string $test_four
 * @property string $test_five
 * @property string $image
 * @property int $test_id
 * @property string $title
 * @property string $description
 * @property boolean $is_text
 *
 * @property Menu $test
 */
class TestQuestion extends ActiveRecord
{
    public $img;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'test_question';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['test_one', 'test_two', 'test_three', 'test_four', 'test_five', 'image', 'img', 'title'], 'string'],
            [['test_id'], 'integer'],
            [['description'], 'safe'],
            [['is_text'], 'boolean'],
            [['test_id'], 'exist', 'skipOnError' => true, 'targetClass' => Menu::className(), 'targetAttribute' => ['test_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'test_one' => 'Правильный ответ',
            'test_two' => 'Ответ#2',
            'test_three' => 'Ответ#3',
            'test_four' => 'Ответ#4',
            'test_five' => 'Ответ#5',
            'image' => 'Картинка',
            'img' => 'Картинка',
            'test_id' => 'Название теста',
            'title' => 'Вопрос',
            'description' => 'Пояснение к ответам',
            'is_text' => 'Ответ текстом?',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTest()
    {
        return $this->hasOne(Menu::className(), ['id' => 'test_id']);
    }
}
