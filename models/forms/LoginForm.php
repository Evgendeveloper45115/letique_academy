<?php

namespace app\models\forms;

use app\models\User;
use Yii;
use yii\base\Model;
use yii\db\Connection;
use yii\helpers\VarDumper;

/**
 * LoginForm is the model behind the login form.
 */
class LoginForm extends Model
{
    public $email;
    public $password;
    public $rememberMe = true;
    public $phone;
    public $_user = false;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['email'], 'required', 'message' => 'Введите корректный e-mail'],
            [['email'], 'email'],
            [['password'], 'required', 'message' => 'Введите пароль'],
            // [['email'], 'email', 'message' => 'Введите корректный e-mail'],
            // rememberMe must be a boolean value
            ['rememberMe', 'boolean'],
            // password is validated by validatePassword()
            ['password', 'validatePassword'],
        ];
    }

    /**
     * Logs in a user using the provided username and password.
     * @return boolean whether the user is logged in successfully
     */
    public function login()
    {
        if ($this->validate()) {
            return Yii::$app->user->login($this->getUser(), 31556926);
        }
        return false;
    }

    /**
     * @return array|bool|null|\yii\db\ActiveRecord
     */
    public function getUser()
    {
        /**
         * @var $user User
         */
        if ($this->_user === false) {
            $this->_user = User::find()->where(['email' => $this->email])->one();
        }
        return $this->_user;
    }

    public function validatePassword($attribute, $params)
    {
        /**
         * @var User $user
         */
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if (!$user || !password_verify($this->password, $user->password)) {
                if ($this->password != $user->password) {
                    $this->addError($attribute, 'Логин или пароль указан с ошибкой');
                }
            }
        }
    }

}
