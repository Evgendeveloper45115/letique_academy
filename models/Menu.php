<?php

namespace app\models;

use app\helpers\myNestedSets;
use app\models\query\MenuQuery;
use creocoder\nestedsets\NestedSetsBehavior;
use Yii;
use yii\behaviors\SluggableBehavior;
use yii\helpers\Inflector;
use yii\helpers\VarDumper;

/**
 * This is the model class for table "menu".
 *
 * @property int $id
 * @property int $lft
 * @property int $rgt
 * @property int $depth
 * @property int $design_id
 * @property int $url
 * @property int $is_news
 * @property string $name
 * @property string $points
 * @property MenuHasUser[] $contentRoles
 * @property Content[] $content
 */
class Menu extends \yii\db\ActiveRecord
{
    public $label;


    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'menu';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'url', 'design_id', 'is_news', 'points'], 'safe'],
            [['lft', 'rgt', 'depth'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'lft' => 'Lft',
            'rgt' => 'Rgt',
            'depth' => 'Depth',
            'name' => 'Название раздела',
            'is_news' => 'В новость',
            'design_id' => 'Шаблон',
            'points' => 'Баллы',
        ];
    }

    public function behaviors()
    {
        return [
            [
                'class' => SluggableBehavior::class,
                //   'attribute' => 'name',
                'slugAttribute' => 'url',//default name slug
                'value' => function ($event) {
                    //  VarDumper::dump($event->parent(),11,1);
                    $slug = Inflector::slug($event->sender->name);
                    //     $rootModel = new Menu(['name' => $event->sender->name, 'url' => $slug]);
                    //    $url = '';
                    //    if(){}
                    return $slug;
                }
            ],

            'tree' => [
                'class' => myNestedSets::class,
                //  'treeAttribute' => 'tree',
                // 'leftAttribute' => 'lft',
                // 'rightAttribute' => 'rgt',
                // 'depthAttribute' => 'depth',
            ],
        ];
    }

    public function transactions()
    {
        return [
            self::SCENARIO_DEFAULT => self::OP_ALL,
        ];
    }

    public static function find()
    {
        return new MenuQuery(get_called_class());
    }

    /**
     * @return Menu
     */
    public static function getMenuByGet()
    {
        return Content::menuSearch();
    }

    public static function getItemsMenuAdmin()
    {
        $root = Menu::findOne(['name' => 'root']);
        if (!$root) {
            /** @var  $rootModel Menu|NestedSetsBehavior */
            $rootModel = new Menu(['name' => 'root', 'url' => '/']);
            $rootModel->makeRoot(); //делаем корневой
            $rootModel->save();
        }

        $new_items = [];
        $new_items2 = [];
        foreach ($root->children(1)->all() as $key => $item) {
            $new_items[$item->id]['url'] = $item->url;
            $new_items[$item->id]['category'] = $item->url;
            foreach ($item->children(1)->all() as $key1 => $item1) {
                $new_items[$item1->id]['url'] = $item->url . '/' . $item1->url;
                $new_items[$item1->id]['category'] = $item1->url;
                foreach ($item1->children(1)->all() as $key2 => $item2) {
                    //  VarDumper::dump($item1, 11, 1);
                    $new_items[$item2->id]['url'] = $item->url . '/' . $item1->url . '/' . $item2->url;
                    $new_items[$item2->id]['category'] = $item2->url;
                    foreach ($item2->children(1)->all() as $key3 => $item3) {
                        //  VarDumper::dump($item1, 11, 1);
                        $new_items[$item3->id]['url'] = $item->url . '/' . $item1->url . '/' . $item2->url . '/' . $item3->url;
                        $new_items[$item3->id]['category'] = $item3->url;
                    }
                }
            }
        }
        foreach ($root->children()->asArray()->all() as $item) {
            $item['url'] = '/admin/category/' . $new_items[$item['id']]['url'];
            $item['icon'] = 'users';
            if ($new_items[$item['id']]['category'] == $_GET['category']) {
                $item['active'] = true;
            } elseif ($new_items[$item['id']]['category'] == $_GET['sub_category']) {
                $item['active'] = true;
            } elseif ($new_items[$item['id']]['category'] == $_GET['children']) {
                $item['active'] = true;
            } elseif ($new_items[$item['id']]['category'] == $_GET['child']) {
                $item['active'] = true;
            }
            $new_items2[] = $item;
        }
        return $new_items2;
    }

    public static function getCategory()
    {
        $menu = \app\models\Menu::find()->where(['url' => $_GET['category']])->one();
        if (isset($_GET['sub_category'])) {
            $sub__cat = \app\models\Menu::find()->where(['url' => $_GET['sub_category']])->one();
            if (!empty($sub__cat->children(1)->all())) {
                $menu = $sub__cat;
                return $menu;
            } else {
                return [$menu];
            }
        }
        if (isset($_GET['child'])) {
            $menu = \app\models\Menu::find()->where(['url' => $_GET['sub_category']])->one();
        }
        return $menu;

    }

    public static function getLastCategory()
    {
        $menu = \app\models\Menu::find()->where(['url' => $_GET['children']])->one();
        if (isset($_GET['child'])) {
            $menu = \app\models\Menu::find()->where(['url' => $_GET['child']])->one();
        }
        return $menu;
    }

    public static function getCategoryMaterialsPage()
    {
        $menu = \app\models\Menu::find()->where(['url' => $_GET['category']])->one();
        if (isset($_GET['sub_category'])) {
            $menu = \app\models\Menu::find()->where(['url' => $_GET['category']])->one();

        }
        if (isset($_GET['child'])) {
            $menu = \app\models\Menu::find()->where(['url' => $_GET['sub_category']])->one();
        }
        return $menu;
    }

    public static function getCategoryTest()
    {
        /**
         * @var $model Menu
         */
        if (isset($_GET['sub_category'])) {
            $model = \app\models\Menu::find()->where(['url' => $_GET['sub_category']])->one();
            if ($model->design_id != 7) {
                return $model;
            }
        }
        return \app\models\Menu::find()->where(['url' => $_GET['category']])->one();


    }

    public static function getCategoryTestSecond()
    {
        /**
         * @var $model Menu
         */
        if (isset($_GET['sub_category'])) {
            $model = \app\models\Menu::find()->where(['url' => $_GET['sub_category']])->one();
            if ($model->design_id != 7) {
                $model = \app\models\Menu::find()->where(['url' => $_GET['children']])->one();
                return $model;
            }
        }
        return \app\models\Menu::find()->where(['url' => $_GET['sub_category']])->one();


    }

    public function getContentRoles()
    {
        return $this->hasMany(MenuHasUser::class, ['menu_id' => 'id']);
    }

    public function getContent()
    {
        return $this->hasMany(Content::class, ['cat' => 'url']);
    }

}
