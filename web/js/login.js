$('.email_send').on('click touch', function (e) {
    e.preventDefault();
    var email = $('.sing_input').val();
    var csrfToken = $('input[name="_csrf"]').val();
    if (email != '') {
        $.ajax({
            url: '/site/forgot-password',
            type: 'post',
            data: {_csrf: csrfToken, email: email},
            success: function (response) {
                console.log(response);
                if (response) {
                    $(document).find('.error').html($(response).find('.error'));
                } else {
                    $(document).find('.login-in-complete').css('display', 'none');
                    $(document).find('.login-complete').css('display', 'block');
                }
            }
        });
    }
});