$(function () {
    $('#countdown').scrollTop(500);
    $('.quest_tab').eq(0).show().addClass('active');
    var answers = [];
    var text = [];
    var total = $('.quest_tab').length;

    var past_time;
    // Number of seconds in every time division
    var days = 24 * 60 * 60,
        hours = 60 * 60,
        minutes = 60;

    // Creating the plugin
    $.fn.countup = function (prop) {

        var options = $.extend({
            callback: function () {
            },
            start: new Date()
        }, prop);

        var passed = 0, d, h, m, s,
            positions;

        // Initialize the plugin
        init(this, options);

        positions = this.find('.position');


        (function tick() {

            passed = Math.floor((new Date() - options.start) / 1000);

            // Number of days passed
            d = Math.floor(passed / days);
            updateDuo(0, 1, d);
            passed -= d * days;

            // Number of hours left
            h = Math.floor(passed / hours);
            updateDuo(2, 3, h);
            passed -= h * hours;

            // Number of minutes left
            m = Math.floor(passed / minutes);
            updateDuo(4, 5, m);
            passed -= m * minutes;

            // Number of seconds left
            s = passed;
            past_time = h + ':' + m + ':' + s;
            updateDuo(6, 7, s);

            // Calling an optional user supplied callback
            options.callback(d, h, m, s);

            // Scheduling another call of this function in 1s
            setTimeout(tick, 1000);
        })();

        // This function updates two digit positions at once
        function updateDuo(minor, major, value) {
            switchDigit(positions.eq(minor), Math.floor(value / 10) % 10);
            switchDigit(positions.eq(major), value % 10);
        }

        return this;
    };


    function init(elem, options) {
        elem.addClass('countdownHolder');
        // Creating the markup inside the container
        $.each(['Days', 'Hours', 'Minutes', 'Seconds'], function (i) {

            $('<span class="count' + this + '">').html(
                '<span class="position">\
                    <span class="digit static">0</span>\
                </span>\
                <span class="position">\
                    <span class="digit static">0</span>\
                </span>'
            ).appendTo(elem);


            if (this != "Seconds") {
                elem.append('<span class="countDiv countDiv' + i + '"></span>');
            }
        });

    }

    // Creates an animated transition between the two numbers
    function switchDigit(position, number) {

        var digit = position.find('.digit')

        if (digit.is(':animated')) {
            return false;
        }

        if (position.data('digit') == number) {
            // We are already showing this number
            return false;
        }

        position.data('digit', number);

        var replacement = $('<span>', {
            'class': 'digit',
            css: {
                top: '-2.1em',
                opacity: 0
            },
            html: number
        });

        // The .static class is added when the animation
        // completes. This makes it run smoother.

        digit
            .before(replacement)
            .removeClass('static')
            .animate({top: '2.5em', opacity: 0}, 'fast', function () {
                digit.remove();
            })

        replacement
            .delay(100)
            .animate({top: 0, opacity: 1}, 'fast', function () {
                replacement.addClass('static');
            });
    }

    $(document).find('#countdown').countup({
        start: new Date() //year, month, day, hour, min, sec
    });
    $('.next').click(function (e) {
        e.preventDefault();
        $(this).attr('style', 'background-color: #10abbb;color:white')
        var index_tab = $('.quest_tab.active').index();
        var val = $('.quest_tab.active').find('input:checked').val();
        if (val || $('.quest_tab.active').find('textarea').val()) {
            flag = true
        } else {
            flag = false
        }

        var valText = null;
        if (typeof val == 'undefined') {
            valText = $('.quest_tab.active').find('textarea').val();
        }
        var id = $('.quest_tab.active').find('input:checked').closest('form').data('id');
        var idText = null;
        if (typeof id == 'undefined') {
            idText = $('.quest_tab.active').find('textarea').closest('form').data('id');
        }
        answers[id] = val;
        text[idText] = valText;
        if (flag) {
            $('.quest_tab.active').removeClass('active').hide();
            var percent = index_tab / total * 100;
            $('.t-footer__progress-bar').attr('style', 'width:' + percent + '%')
            if (percent == 100) {
                $('.t-footer__progress__circle').addClass('active')

            }
            console.log(index_tab);
            if (index_tab <= (total - 1)) {
                $('.quest_tab').eq(index_tab).show().addClass('active');


            } else {
                $.ajax({
                    type: "POST",
                    url: '/cabinet/test',
                    data: {answers: answers, text: text, id: $('.cat_id').attr('data-value')},
                    success: function (res) {
                        var q = JSON.parse(res);
                        if (q.success === true) {
                            window.location.href = "/cabinet/test-results?cat_id=" + $('.cat_id').attr('data-value');
                        }
                    },
                    error: function (error) {
                    }
                });
                return false;
            }
        }

    });


    function getParameterByName(name, href) {
        name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
        var regexS = "[\\?&]" + name + "=([^&#]*)";
        var regex = new RegExp(regexS);
        var results = regex.exec(href);
        if (results == null)
            return "";
        else
            return decodeURIComponent(results[1].replace(/\+/g, " "));
    }

});


