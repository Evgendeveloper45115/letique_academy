const $headerButton = document.querySelector('.header__button')
$headerButton.addEventListener('click', () => {
    document.querySelector('.header__username-menu').classList.toggle('display-none')
    $headerButton.classList.toggle('active')
})

if (document.querySelector('.courses__lesson-list') && document.querySelector('.courses__inner-nav-list')) {
    document.querySelector('.main-nav').classList.add('main-nav--lesson')
}

if (!document.querySelector('.courses__lesson-list') && !document.querySelector('.courses__inner-nav-list')) {
    document.querySelector('.main-nav').classList.add('main-nav--one')
}

if (document.getElementById('mob-modal')) {
    const $mobileMenu = document.getElementById('mob-modal')
    const $mobileMenuOpenButton = document.getElementById('mobile-menu-open')
    const $mobileMenuCloseButton = document.getElementById('mob-modal__close')

    $mobileMenu.classList.add('display-none')
    $mobileMenuOpenButton.addEventListener('click', () => $mobileMenu.classList.remove('display-none'))
    $mobileMenuCloseButton.addEventListener('click', () => $mobileMenu.classList.add('display-none'))

    const $mobileMenuItemsArray = document.querySelectorAll('.mob-modal__item')
    for (let i = 0; i < $mobileMenuItemsArray.length; i++) {
        if ($mobileMenuItemsArray[i].querySelector('ul')) {
            $mobileMenuItemsArray[i].classList.add('js--mob-modal')
        }
    }

    const $mobileMenuItemWithUlArray = $mobileMenu.querySelectorAll('.js--mob-modal > a')
    const $mobileMenuList2Array = $mobileMenu.querySelectorAll('.mob-modal__list2')

    $mobileMenuList2Array.forEach(item => item.classList.add('display-none'))

    const mobileMenuItemWithUlArrayHandler = function (i) {
        return function (event) {
            event.preventDefault()
            $mobileMenuList2Array[i].classList.toggle('display-none')
        }
    }

    $mobileMenuItemWithUlArray.forEach((item, index) => item.addEventListener('click', mobileMenuItemWithUlArrayHandler(index)))
}

// if (document.querySelector('.main-nav__list')) {
//     const $navigationItemWithUlArray = $mobileMenu.querySelectorAll('.js--mob-modal > a')
//     const $navigationList2Array = $mobileMenu.querySelectorAll('.mob-modal__list2')

//     $navigationList2Array.forEach(item => item.classList.add('display-none'))

//     const navigationItemWithUlArrayHandler = function (i) {
//         return function(event) {
//         event.preventDefault()
//         $navigationList2Array[i].classList.toggle('display-none')
//         }
//     }

//     $navigationItemWithUlArray.forEach((item, index) => item.addEventListener('click', mobileMenuItemWithUlArrayHandler(index)))
// }

if (document.querySelector(".download-doc__show")) {
    var changingGrid = document.querySelector(".download-doc__show");
    var buttonCards = document.querySelector(".download-doc__show-cards");
    var buttonList = document.querySelector(".download-doc__show-list");
    var extContainersLow = document.querySelectorAll(".js-low");
    var extContainersMedium = document.querySelectorAll(".js-medium");
    var extContainersHigh = document.querySelectorAll(".js-high");
    var dowloadDocs = document.querySelectorAll(".download-doc__item");

    changingGrid.addEventListener("click", function () {
        for (var j = 0; j < extContainersLow.length; j++) {
            extContainersLow[j].classList.toggle("download-doc__ext--card-low");
        }
        for (var k = 0; k < extContainersMedium.length; k++) {
            extContainersMedium[k].classList.toggle("download-doc__ext--card-medium");
        }
        for (var h = 0; h < extContainersHigh.length; h++) {
            extContainersHigh[h].classList.toggle("download-doc__ext--card-high");
        }
        for (var g = 0; g < dowloadDocs.length; g++) {
            dowloadDocs[g].classList.toggle("download-doc__item--material");
        }
        buttonList.classList.toggle("download-doc__show-active");
        buttonCards.classList.toggle("download-doc__show-active");
    });
}

if (document.getElementById('drop-area')) {
    let dropArea = document.getElementById('drop-area')
    let filesDone = 0
    let filesToDo = 0
    let progressBar = document.getElementById('progress-bar')
    let avaPreview = document.getElementById("avaPreview")
    let inputFile = document.getElementById("fileElem")

    ;['dragenter', 'dragover', 'dragleave', 'drop'].forEach(eventName => {
        dropArea.addEventListener(eventName, preventDefaults, false)
    })

    function preventDefaults(e) {
        e.preventDefault()
        e.stopPropagation()
    }

    let clearImg = function () {
            while (avaPreview.firstChild) {
                avaPreview.removeChild(avaPreview.firstChild)
            }
        }

    ;['dragenter', 'dragover'].forEach(eventName => {
        dropArea.addEventListener(eventName, highlight, false)
    })
    ;['dragleave', 'drop'].forEach(eventName => {
        dropArea.addEventListener(eventName, unhighlight, false)
    })

    function highlight(e) {
        dropArea.classList.add('account__ava-container--js')
    }

    function unhighlight(e) {
        dropArea.classList.remove('account__ava-container--js')
    }

    dropArea.addEventListener('drop', handleDrop, false)

    function handleDrop(e) {
        let dt = e.dataTransfer
        let files = dt.files
        handleFiles(files)
    }

    function handleFiles(files) {
        files = [...files]
        initializeProgress(files.length)
        files.forEach(uploadFile)
        files.forEach(previewFile)
    }

    function uploadFile(file) {
        let url = 'ВАШ URL ДЛЯ ЗАГРУЗКИ ФАЙЛОВ'
        let formData = new FormData()
        formData.append('file', file)
        fetch(url, {
            method: 'POST',
            body: formData
        })
            .then((progressDone) => { /* Готово. Информируем пользователя */
            })
            .catch(() => { /* Ошибка. Информируем пользователя */
            })
    }

    function previewFile(file) {
        clearImg()
        let reader = new FileReader()
        reader.readAsDataURL(file)
        reader.onloadend = function () {
            let img = document.createElement('img')
            img.src = reader.result
            document.getElementById('avaPreview').appendChild(img)
        }
    }

    function initializeProgress(numfiles) {
        progressBar.value = 0
        filesDone = 0
        filesToDo = numfiles
    }

    function progressDone() {
        filesDone++
        progressBar.value = filesDone / filesToDo * 100
    }
}
;

if (document.querySelector(".account__shevron")) {
    var shevronButton = document.querySelector(".account__shevron");
    var accountFormContainer = document.querySelector(".account__form-container");
    var account = document.querySelector(".account");

    shevronButton.addEventListener("click", function () {
        accountFormContainer.classList.toggle("visually-hidden");
        shevronButton.classList.toggle("account__shevron--js");
        account.classList.toggle("account--js");
    })
}
;

if (document.querySelector(".main-nav") && !document.querySelector(".courses__progress-item") && !document.querySelector(".main-nav--lesson") && !document.querySelector(".activity__container") && !document.querySelector(".download-doc") && !document.querySelector(".news__container")) {
    var mainContent = document.querySelector(".main-content")
    var mainContantButton = document.querySelector(".main-content__button");
    var navigationBar = document.querySelector(".main-nav");
    mainContantButton.addEventListener("click", function () {
        mainContent.classList.toggle("main-content--js");
        navigationBar.classList.toggle("main-nav--hide-menu");
        mainContantButton.classList.toggle("main-content__button--hide-menu");
    });
}
;

if (document.querySelector(".courses__progress-item") && !document.querySelector(".main-nav--lesson") && !document.querySelector(".activity__container") && !document.querySelector(".download-doc") && !document.querySelector(".news__container")) {
    var mainContent = document.querySelector(".main-content")
    var mainContentButton = document.querySelector(".main-content__button");
    var navigationBar = document.querySelector(".main-nav");
    var coursesItem = document.querySelectorAll(".courses__progress-item");
    var moduls = document.querySelector(".moduls");
    var coursesNavList = document.querySelector(".courses__nav-list");
    var coursesInnerNavList = document.querySelector(".courses__inner-nav-list");
    var coursesLessonList = document.querySelector(".courses__lesson-list");
    mainContentButton.addEventListener("click", function () {
        mainContent.classList.toggle("main-content--js");
        navigationBar.classList.toggle("main-nav--hide-menu");
        mainContentButton.classList.toggle("main-content__button--hide-menu");
        for (var i = 0; i < coursesItem.length; i++) {
            coursesItem[i].classList.toggle("courses__progress-item--js");
        }
        ;
        moduls.classList.toggle("moduls--js");
        coursesNavList.classList.toggle("courses__nav-list--js");
        coursesInnerNavList.classList.toggle("courses__inner-nav-list--js");
        coursesLessonList.classList.toggle("courses__lesson-list--js");
    });
}

if (document.querySelector(".main-nav--lesson") && !document.querySelector(".activity__container") && !document.querySelector(".download-doc") && !document.querySelector(".news__container")) {
    var mainContent = document.querySelector(".main-content")
    var mainContentButton = document.querySelector(".main-content__button");
    var navigationBar = document.querySelector(".main-nav");
    var coursesItem = document.querySelectorAll(".courses__progress-item");
    var coursesInnerNavList = document.querySelector(".courses__inner-nav-list");
    var mainNavLesson = document.querySelector(".main-nav--lesson");
    var coursesLessonList = document.querySelector(".courses__lesson-list");
    mainContentButton.addEventListener("click", function () {
        mainContent.classList.toggle("main-content--js");
        navigationBar.classList.toggle("main-nav--hide-menu");
        mainContentButton.classList.toggle("main-content__button--hide-menu");
        for (var i = 0; i < coursesItem.length; i++) {
            coursesItem[i].classList.toggle("courses__progress-item--js");
        }
        ;
        coursesInnerNavList.classList.toggle("courses__inner-nav-list--js");
        coursesLessonList.classList.toggle("courses__lesson-list--js");
        mainNavLesson.classList.toggle("main-nav--lesson-js")
    });
}

if (document.querySelector(".activity__container") && !document.querySelector(".download-doc") && !document.querySelector(".news__container")) {
    var mainContent = document.querySelector(".main-content")
    var mainContentButton = document.querySelector(".main-content__button");
    var navigationBar = document.querySelector(".main-nav");
    var coursesItem = document.querySelectorAll(".courses__progress-item");
    var activityContainer = document.querySelector(".activity__container");
    mainContentButton.addEventListener("click", function () {
        mainContent.classList.toggle("main-content--js");
        navigationBar.classList.toggle("main-nav--hide-menu");
        mainContentButton.classList.toggle("main-content__button--hide-menu");
        for (var i = 0; i < coursesItem.length; i++) {
            coursesItem[i].classList.toggle("courses__progress-item--js");
        }
        ;
        activityContainer.classList.toggle("activity__container--js");
    });
}

if (document.querySelector(".download-doc") && !document.querySelector(".news__container")) {
    var mainContent = document.querySelector(".main-content")
    var mainContentButton = document.querySelector(".main-content__button");
    var navigationBar = document.querySelector(".main-nav");
    var coursesItem = document.querySelectorAll(".courses__progress-item");
    var activityContainer = document.querySelector(".activity__container");
    var dowloadDocInnerActivity = document.querySelector(".download-doc")
    mainContentButton.addEventListener("click", function () {
        mainContent.classList.toggle("main-content--js");
        navigationBar.classList.toggle("main-nav--hide-menu");
        mainContentButton.classList.toggle("main-content__button--hide-menu");
        for (var i = 0; i < coursesItem.length; i++) {
            coursesItem[i].classList.toggle("courses__progress-item--js");
        }
        ;
        activityContainer.classList.toggle("activity__container--js");
        dowloadDocInnerActivity.classList.toggle("download-doc--inner-activity");
    });
}

if (document.querySelector(".news__container")) {
    var mainContent = document.querySelector(".main-content")
    var mainContentButton = document.querySelector(".main-content__button");
    var navigationBar = document.querySelector(".main-nav");
    var coursesItem = document.querySelectorAll(".courses__progress-item");
    var newsContainer = document.querySelector(".news__container");
    var coursesInnerNavList = document.querySelector(".courses__inner-nav-list");

    mainContentButton.addEventListener("click", function () {
        mainContent.classList.toggle("main-content--js");
        navigationBar.classList.toggle("main-nav--hide-menu");
        mainContentButton.classList.toggle("main-content__button--hide-menu");
        for (var i = 0; i < coursesItem.length; i++) {
            coursesItem[i].classList.toggle("courses__progress-item--js");
        }
        ;
        newsContainer.classList.toggle("news__container--js");
        coursesInnerNavList.classList.toggle("courses__inner-nav-list--js");
    });
}

if (document.querySelector('.video')) {
    function findVideos() {
        let videos = document.querySelectorAll('.video');

        for (let i = 0; i < videos.length; i++) {
            setupVideo(videos[i]);
        }
    }

    function setupVideo(video) {
        let link = video.querySelector('.video__link');
        let media = video.querySelector('.video__media');
        let button = video.querySelector('.video__button');
        let id = parseMediaURL(media);

        video.addEventListener('click', () => {
            let iframe = createIframe(id);

            link.remove();
            button.remove();
            video.appendChild(iframe);
        });

        link.removeAttribute('href');
        video.classList.add('video--enabled');
    }

    function parseMediaURL(media) {
        let regexp = /https:\/\/i\.ytimg\.com\/vi\/([a-zA-Z0-9_-]+)\/maxresdefault\.jpg/i;
        let url = media.src;
        let match = url.match(regexp);

        return match[1];
    }

    function createIframe(id) {
        let iframe = document.createElement('iframe');

        iframe.setAttribute('allowfullscreen', '');
        iframe.setAttribute('src', generateURL(id));
        iframe.classList.add('video__media');

        return iframe;
    }

    function generateURL(id) {
        let query = '?rel=0&showinfo=0&autoplay=1&mute=1';

        return 'https://www.youtube.com/embed/' + id + query;
    }

    findVideos();
}
