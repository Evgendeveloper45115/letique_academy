<?php

namespace app\helpers;


use app\components\MyUrlManager;
use app\models\ContentType;
use app\models\EveryDay;
use app\models\Group;
use app\models\User;
use Imagine\Image\Box;
use Imagine\Image\ManipulatorInterface;
use Imagine\Imagick\Imagine;
use pendalf89\imageresizer\Image;
use pendalf89\imageresizer\ImageResizer;
use phpDocumentor\Reflection\Types\Self_;
use yii\db\Connection;
use yii\helpers\VarDumper;

class My extends ImageResizer
{
    static public function getClassLayout()
    {
        $contentDesign = \app\models\Menu::find()->where(['url' => $_GET['category']])->one();
        if ($contentDesign->design_id == 2 || $contentDesign->design_id == 4) {
            return 'main-nav--lesson';
        }
    }

    static public function getUrl()
    {
        $url = $_GET['category'] . '/' . $_GET['sub_category'] . '/' . $_GET['children'] . '/' . $_GET['child'];
        if (count($_GET) == 4) {
            $url = $_GET['category'] . '/' . $_GET['sub_category'] . '/' . $_GET['children'];
        }
        if (count($_GET) == 3) {
            $url = $_GET['category'] . '/' . $_GET['sub_category'];
        }
        if (count($_GET) == 2) {
            $url = $_GET['category'];
        }
        return $url;
    }

    static function sendEmail($user, $view, $subject, $password = null)
    {
        $file = \Yii::getAlias('@runtime') . '/logs/' . date('Y-m-d') . 'log-email-sender.txt';
        if (!is_file($file)) {
            fopen($file, "w", true);
        }
        $post_value = [
            'api_key' => '69e8txindd4yyjiiu3h3ftu6rg4oh7y7ptogy3ya',
            'domain' => 'dev-academy.letique.ru',
            'message' => [
                'body' => [
                    'html' => \Yii::$app->controller->renderPartial('@app/mail/' . $view, ['user' => $user, 'password' => $password])
                ],
                'subject' => $subject,
                'from_email' => 'admin@letique.ru',
                'from_name' => 'letique',
                'recipients' => [
                    [
                        'email' => $user->email,
                    ],
                ],
            ],
        ];
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://eu1.unione.io/ru/transactional/api/v1/email/send.json');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post_value));
        $result = curl_exec($ch);
        curl_close($ch);
        file_put_contents($file, PHP_EOL . $result, FILE_APPEND);
        return $result;
    }

}