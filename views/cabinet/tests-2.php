<?php
/**
 * @var $models \app\models\Content[]
 */

$menu = \app\models\Menu::getCategoryTest();
$children = [];
?>
<section class="courses">
    <h1 class="courses__title">Тестирование</h1>
    <?php
    if ($menu) {
        ?>
        <ul class="courses__inner-nav-list courses__inner-nav-list--material">
            <?php
            foreach ($menu->children(1)->all() as $item) {
                ?>
                <li class="courses__inner-item">
                    <a href="/cabinet/<?= $_GET['category'] ?>/<?= $_GET['sub_category'] ?>/<?= $item->url ?>"
                       class="courses__inner-link courses__inner-link<?= $item->url == $_GET['sub_category'] || $item->url == $_GET['children'] ? ' courses__inner-link--current' : null ?>"><?= preg_replace('/(.*)\-\d{1,2}/', '$1', $item->name) ?></a>
                </li>
                <?php
            }
            ?>
        </ul>
        <?php
    }
    $menu_sec = \app\models\Menu::getCategoryTestSecond();
    if ($menu_sec && !empty($menu_sec->children(2)->all())) {
        ?>
        <ul class="courses__lesson-list">
            <?php
            foreach ($menu_sec->children(1)->all() as $child) {

                ?>
                <li class="courses__lesson-item">
                    <a href="/cabinet/<?= $_GET['category'] ?>/<?= $_GET['sub_category'] ?>/<?= $_GET['children'] ?>/<?= $child->url ?>"
                       class="courses__lesson-link<?= $child->url == $_GET['child'] ? '--current' : null ?>"><?= preg_replace('/(.*)\-\d{1,2}/', '$1', $child->name) ?></a>
                </li>
                <?php
            }
            ?>
        </ul>
        <?php
    }
    $menu_sec = \app\models\Content::menuSearch();
    $content = $menu_sec->leaves()->all();
    if (empty($content)) {
        $content = [$menu_sec];
    }
    if ($content) {
        ?>
        <div class="activity__container">
            <?php
            foreach ($content as $model) {

                ?>
                <a href="/cabinet/<?= $_GET['category'] ?>/<?= $_GET['sub_category'] ?>/<?= $_GET['children'] ?>/<?= $model->url ?>"
                   class="activity__card">
                    <img src="/uploads/<?= 2 ?>/<?= 32 ?>"
                         alt="<?= 23 ?>"
                         class="activity__img">
                    <h2 class="activity__card-title"><?= $model->name ?></h2>
                    <?php
                    if (true) {
                        ?>
                        <p class="activity__date">
                            Завершен
                        </p>
                        <?php
                    } else {
                        ?>
                        <p class="activity__date">
                            Надо набрать бы балов
                        </p>
                        <?php
                    }
                    ?>
                    <span class="activity__location"><?= 123 ?></span>
                </a>
                <?php
            }
            ?>
        </div>
        <?php
    }

    ?>
</section>

