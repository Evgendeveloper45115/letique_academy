<?php
/**
 * @var $models \app\models\Content[]
 */

$menu = \app\models\Menu::getCategoryMaterialsPage();
$children = [];
?>
<section class="courses">
    <h1 class="courses__title">Материалы</h1>
    <?php
    if ($menu) {
        ?>
        <ul class="courses__inner-nav-list courses__inner-nav-list--material">
            <?php
            foreach ($menu->children(1)->all() as $item) {
                if (isset($_GET['child'])) {
                    ?>
                    <li class="courses__inner-item">
                        <a href="/cabinet/<?= $_GET['category'] ?>/<?= $_GET['sub_category'] ?>/<?= $item->url ?>"
                           class="courses__inner-link courses__inner-link<?= $item->url == $_GET['sub_category'] || $item->url == $_GET['children'] ? ' courses__inner-link--current' : null ?>"><?= preg_replace('/(.*)\-\d{1,2}/', '$1', $item->name) ?></a>
                    </li>
                    <?php
                } else {
                    ?>
                    <li class="courses__inner-item">
                        <a href="/cabinet/<?= $_GET['category'] ?>/<?= $item->url ?>"
                           class="courses__inner-link courses__inner-link<?= $item->url == $_GET['sub_category'] || $item->url == $_GET['children'] ? ' courses__inner-link--current' : null ?>"><?= preg_replace('/(.*)\-\d{1,2}/', '$1', $item->name) ?></a>
                    </li>
                    <?php
                }

                ?>
                <?php
            }
            ?>
        </ul>
        <?php
    }
    $menu_sec = \app\models\Menu::find()->where(['url' => isset($_GET['child']) ? $_GET['children'] : $_GET['sub_category']])->one();
    if ($menu_sec) {
        ?>
        <ul class="courses__lesson-list">
            <?php
            foreach ($menu_sec->children(1)->all() as $child) {
                ?>
                <?php
                if (isset($_GET['child'])) {
                    ?>
                    <li class="courses__lesson-item">
                        <a href="/cabinet/<?= $_GET['category'] ?>/<?= $_GET['sub_category'] ?>/<?= $_GET['children'] ?>/<?= $child->url ?>"
                           class="courses__lesson-link<?= $child->url == $_GET['child'] ? '--current' : null ?>"><?= preg_replace('/(.*)\-\d{1,2}/', '$1', $child->name);
                            ?></a>
                    </li>
                    <?php
                } else {
                    ?>
                    <li class="courses__lesson-item">
                        <a href="/cabinet/<?= $_GET['category'] ?>/<?= $_GET['sub_category'] ?>/<?= $child->url ?>"
                           class="courses__lesson-link<?= $child->url == $_GET['children'] ? '--current' : null ?>"><?= preg_replace('/(.*)\-\d{1,2}/', '$1', $child->name) ?></a>
                    </li>
                    <?php
                }
                ?>
                <?php
            }
            ?>
        </ul>
        <?php
    }

    ?>
</section>
<section class="download-doc">
    <div class="download-doc__show">
        <span class="download-doc__show-text">Показать</span>
        <button class="download-doc__show-cards download-doc__show-active" title="показать карточками">Карточками
        </button>
        <button class="download-doc__show-list" title="показать списком">Списком</button>
    </div>
    <?php
    if (!empty($models)) {
        ?>
        <ul class="download-doc__list download-doc__list--material">
            <?php
            foreach ($models as $model) {
                if ($model->video) {
                    ?>
                    <li class="download-doc__item download-doc__item--material">
                        <div class="download-doc__ext download-doc__ext--card-low js-low">
                            <div class="video">
                                <a class="video__link" href="https://youtu.be/<?= $model->video ?>">
                                    <picture>
                                        <source
                                            srcset="https://i.ytimg.com/vi_webp/<?= $model->video ?>/maxresdefault.webp"
                                            type="image/webp">
                                        <img class="video__media"
                                             src="https://i.ytimg.com/vi/<?= $model->video ?>/maxresdefault.jpg"
                                             alt="<?= $model->name ?>">
                                    </picture>
                                </a>
                                <button class="video__button" aria-label="Запустить видео">
                                    <svg width="68" height="48" viewBox="0 0 68 48">
                                        <path class="video__button-shape"
                                              d="M66.52,7.74c-0.78-2.93-2.49-5.41-5.42-6.19C55.79,.13,34,0,34,0S12.21,.13,6.9,1.55 C3.97,2.33,2.27,4.81,1.48,7.74C0.06,13.05,0,24,0,24s0.06,10.95,1.48,16.26c0.78,2.93,2.49,5.41,5.42,6.19 C12.21,47.87,34,48,34,48s21.79-0.13,27.1-1.55c2.93-0.78,4.64-3.26,5.42-6.19C67.94,34.95,68,24,68,24S67.94,13.05,66.52,7.74z"></path>
                                        <path class="video__button-icon" d="M 45,24 27,14 27,34"></path>
                                    </svg>
                                </button>
                            </div>
                            <span
                                class="download-doc__ext-text"><?= pathinfo($model->images[0]->name)['extension'] ?></span>
                        </div>
                        <div>
                            <h3 class="download-doc__item-title"><?= $model->name ?></h3>
                            <?php
                            if ($model->images) {
                                ?>
                                <a href="/uploads/<?= $model->menu->id ?>/<?= $model->images[0]->name ?>"
                                   class="download-doc__link">Скачать
                                    документ</a>
                                <?php
                            }
                            ?>

                        </div>
                    </li>
                    <?php
                } else {
                    ?>
                    <li class="download-doc__item download-doc__item--material">
                        <div class="download-doc__ext --card-high js-high download-doc__ext--card-high">
                            <img src="/uploads/<?= $model->menu->id ?>/<?= $model->image ?>" alt=""
                                 class="download-doc__ext-img">
                            <?php
                            if ($model->images) {
                                ?>
                                <span
                                    class="download-doc__ext-text"><?= pathinfo($model->images[0]->name)['extension'] ?></span>
                                <?php
                            }
                            ?>
                        </div>
                        <div>
                            <h3 class="download-doc__item-title"><?= $model->name ?></h3>
                            <?php
                            if ($model->images) {
                                ?>
                                <a href="/uploads/<?= $model->menu->id ?>/<?= $model->images[0]->name ?>"
                                   class="download-doc__link"
                                   download="<?= $model->name ?>">Скачать
                                    документ</a>
                                <?php
                            }
                            ?>

                        </div>
                    </li>
                    <?php
                }
                ?>

                <?php
            }
            ?>
        </ul>
        <?php
    }
    ?>
</section>

