<?php
/**
 * @var $menu \app\models\Menu
 * @var $tests \app\models\TestQuestion[]
 */
?>

<main class="main--test"> <!-- внимание, здесь на main класс который необходим для вёрстки -->

    <div style="display: none" class="cat_id" data-value="<?= $menu->id ?>"></div>
    <section class="test">
        <h1 class="test__title"><?= $menu->name ?></h1>
        <?php
        if ($tests) {
            foreach ($tests as $key => $question) {
                $answers = [];
                if (!$question->is_text) {
                    $answers[] = '<label class="test__variant">
                    <input id="an2-' . $key . 1 . '" type="radio" class="visually-hidden" name="answer" value="' . $question->test_one . '"><span
                            class="test__custom-radio"></span>
                    ' . $question->test_one . '
                </label>';
                    $answers[] = '<label class="test__variant">
                    <input id="an2-' . $key . 2 . '" type="radio" class="visually-hidden" name="answer" value="' . $question->test_two . '"><span
                            class="test__custom-radio"></span>
                    ' . $question->test_two . '
                </label>';
                    if ($question->test_three) {
                        $answers[] = '<label class="test__variant">
                    <input id="an2-' . $key . 3 . '" type="radio" class="visually-hidden" name="answer" value="' . $question->test_three . '"><span
                            class="test__custom-radio"></span>
                    ' . $question->test_three . '
                </label>';
                    }
                    if ($question->test_four) {
                        $answers[] = '<label class="test__variant">
                    <input id="an2-' . $key . 4 . '" type="radio" class="visually-hidden" name="answer" value="' . $question->test_four . '"><span
                            class="test__custom-radio"></span>
                    ' . $question->test_four . '
                </label>';
                    }
                    if ($question->test_five) {
                        $answers[] = '<label class="test__variant">
                    <input id="an2-' . $key . 5 . '" type="radio" class="visually-hidden" name="answer" value="' . $question->test_five . '"><span
                            class="test__custom-radio"></span>
                    ' . $question->test_five . '
                </label>';
                    }

                } else {
                    $answers[] = ' <textarea id="an2-' . $key . 1 . '" name="test__textarea" id="" cols="60" rows="10"></textarea>';
                }
                shuffle($answers);
                ?>
                <div class="quest_tab">
                    <form action="" class="test__form" id="test-answer-<?= $key ?>" data-id="<?= $question->id ?>">
                        <p class="test__question"><?= $question->title ?></p>
                        <div class="test__variants">
                            <?php
                            foreach ($answers as $answer) {
                                echo $answer;
                            }
                            ?>
                        </div>
                        <a href="#" class="blue-border-button next">Слeдующий вопрос</a>
                    </form>

                </div>

                <?php
            }
        }
        ?>
    </section>

    <!-- Секция с поздравлениями об окончинии, сейчас скрыта -->
    <section class="test" style="display: none;"><!-- скрыта здесь с помощью style="display: none;" -->
        <h1 class="test__title">Тест на знание продукции Letique часть I</h1>
        <p class="test__congrat">Поздравляем! Тест пройден!</p>
        <p class="test__of">48/48</p>
        <p class="test__you-are-ready">Вы готовы к работе</p>
        <a href="" class="blue-border-button test__another">Пройти другой тест</a>
    </section>

    <footer class="t-footer">
        <div class="t-footer__progress-container">
            <div class="t-footer__progress-under-bar">
                <div class="t-footer__progress-bar" style="width: 0%;"></div>
                <!-- Здесь меняются проценты, сейчас 25% стоит -->
            </div>
            <div class="t-footer__progress__circle"></div>
            <!-- Здесь круглику добавляем класс active и он станет синий -->
        </div>
        <span class="t-footer__of">1/48</span>
        <a href="#" class="t-footer__link">Выйти</a>
    </footer>

    <!-- Картинка девушки -->
    <picture>
        <source media="(max-width: 750px)"
                srcset="/images/test-girl-mobile.png 1x, /images/test-girl-mobile@2x.png 2x, /images/test-girl-mobile@3x.png 3x">
        <img class="test__img"
             srcset="/images/test-girl@2x.png 2x, /images/test-girl@3x.png 3x"
             src="/images/test-girl.png"
             alt="Девушка со смартфоном"
        >
    </picture>

</main>

