<?php
/**
 * @var $model \app\models\Content
 * @var $prevModel \app\models\Content
 * @var $nextModel \app\models\Content
 */
?>
    <section class="inner-activity">
        <h1 class="inner-activity__title"><?= $model->name ?></h1>
        <img src="/uploads/<?= $model->menu->id ?>/<?= $model->image ?>"
             alt="<?= $model->image ?>" class="inner-activity__img">
        <?= $model->description ?>
    </section>
<?php
if ($model->images) {
    ?>
    <section class="download-doc">
        <h2 class="download-doc__title">Скачайте документы:</h2>
        <ul class="download-doc__list">
            <?php
            foreach ($model->images as $image) {
                ?>

                <li class="download-doc__item">
                    <div class="download-doc__ext">
                        <span class="download-doc__ext-text"><?= pathinfo($image->name)['extension'] ?></span>
                    </div>
                    <div>
                        <h3 class="download-doc__item-title"><?= $image->title ?></h3>
                        <a href="/uploads/<?= $model->menu->id ?>/<?= $image->name ?>"
                           class="download-doc__link"
                           download="/uploads/<?= $model->menu->id ?>/<?= $image->name ?>">Скачать документ</a>
                    </div>
                </li>
                <?php
            }
            ?>
        </ul>
    </section>
    <?php
}
?>
<?php
if ($prevModel || $nextModel) {
    ?>
    <section class="activity">
        <div class="activity__container">
            <?php
            if ($prevModel) {
                ?>
                <a href="/cabinet/<?= $_GET['category'] ?>/<?= $prevModel->id ?>" class="activity__card">
                    <img src="/uploads/<?= $_GET['category'] ?>/<?= $prevModel->image ?>" alt="<?= $prevModel->name ?>"
                         class="activity__img">
                    <h2 class="activity__card-title"><?= $prevModel->name ?></h2>
                    <p class="activity__date">
                        до <?= date('d', strtotime($prevModel->date_to)) . ' ' . \app\models\Content::getMonths($prevModel->date_to) . ' ' . date('Y', strtotime($prevModel->date_to)) ?>
                    </p>

                    <span class="activity__location"><?= $prevModel->location ?></span>
                </a>
                <?php
            }
            if ($nextModel) {
                ?>
                <a href="/cabinet/<?= $_GET['category'] ?>/<?= $nextModel->id ?>" class="activity__card">
                    <img src="/uploads/<?= $_GET['category'] ?>/<?= $nextModel->image ?>" alt="<?= $nextModel->name ?>"
                         class="activity__img">
                    <h2 class="activity__card-title"><?= $nextModel->name ?></h2>
                    <p class="activity__date">
                        до <?= date('d', strtotime($nextModel->date_to)) . ' ' . \app\models\Content::getMonths($nextModel->date_to) . ' ' . date('Y', strtotime($nextModel->date_to)) ?>
                    </p>

                    <span class="activity__location"><?= $nextModel->location ?></span>
                </a>
                <?php
            }
            ?>
        </div>
    </section>
    <?php
}