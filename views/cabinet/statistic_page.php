<?php
$root = \app\models\Menu::find()->where(['url' => $_GET['category']])->one();
?>

<section class="courses">
    <h1 class="courses__title">Обучающий курс</h1>
    <?php
    if ($root) {
        ?>
        <ul class="courses__inner-nav-list courses__inner-nav-list--material">
            <?php
            foreach ($root->children(1)->all() as $cat) {
                $two_child = \app\models\Menu::findOne(['url' => 'category/' . $_GET['category']]);
                ?>
                <li class="courses__inner-item">
                    <a href="/cabinet/<?= $_GET['category'] ?>/<?= $cat->url ?>"
                       class="courses__link"><?= $cat->name ?></a>
                </li>
                <?php
            }

            ?>
        </ul>
        <?php
    }
    ?>
    <div class="courses__info-container">
        <div class="courses__info-wrapper">
            <div class="courses__info">
                <span class="courses__info-text">Пройдено:</span>
                <span class="courses__info-percent">19%</span>
            </div>
            <div class="courses__info">
                <span class="courses__info-text">Осталось:</span>
                <span class="courses__info-percent">81%</span>
            </div>
        </div>
        <button class="courses__btn blue-border-button">Продолжить обучение</button>
    </div>
    <ul class="courses__progress-list">
        <li class="courses__progress-item courses__progress-item--completed"></li>
        <li class="courses__progress-item courses__progress-item--completed"></li>
        <li class="courses__progress-item"></li>
        <li class="courses__progress-item"></li>
        <li class="courses__progress-item"></li>
        <li class="courses__progress-item"></li>
        <li class="courses__progress-text">Готово!</li>
    </ul>
</section>

<section class="moduls">
    <div class="modul-card">
        <h2 class="modul-card__title">О бренде</h2>
        <!-- Чтобы поставить галочку, вместо числа с процентом - удалите число внутри спана и добавьте класс "modul-card__percent--completed" -->
        <span class="modul-card__percent modul-card__percent--completed"></span>
        <p class="modul-card__passed-users">
            Прошли модуль: 957
            <i class="modul-card__passed-icon"></i>
        </p>
        <a href="#" class="modul-card__link">Перейти в раздел</a>
        <div class="modul-card__bar">
            <!-- Чтобы изменить процент заполнения шкалы - измените style="width: %" на нужное значение -->
            <div class="modul-card__bar-completed" style="width: 100%"></div>
        </div>
        <div class="modul-card__score-container">
            <span class="modul-card__required">Обязательный</span>
            <span class="modul-card__score">+5 баллов</span>
        </div>
    </div>

    <div class="modul-card">
        <h2 class="modul-card__title">Уход за кожей</h2>
        <!-- Чтобы поставить галочку, вместо числа с процентом - удалите число внутри спана и добавьте класс "modul-card__percent--completed" -->
        <span class="modul-card__percent">78%</span>
        <p class="modul-card__passed-users">
            Прошли модуль: 957
            <i class="modul-card__passed-icon"></i>
        </p>
        <a href="#" class="modul-card__link">Перейти в раздел</a>
        <div class="modul-card__bar">
            <!-- Чтобы изменить процент заполнения шкалы - измените style="width: %" на нужное значение -->
            <div class="modul-card__bar-completed" style="width: 78%"></div>
        </div>
        <div class="modul-card__score-container">
            <span class="modul-card__required">Обязательный</span>
            <span class="modul-card__score">+5 баллов</span>
        </div>
    </div>

    <div class="modul-card">
        <h2 class="modul-card__title">Ассортимент</h2>
        <!-- Чтобы поставить галочку, вместо числа с процентом - удалите число внутри спана и добавьте класс "modul-card__percent--completed" -->
        <span class="modul-card__percent">22%</span>
        <p class="modul-card__passed-users">
            Прошли модуль: 957
            <i class="modul-card__passed-icon"></i>
        </p>
        <a href="#" class="modul-card__link">Перейти в раздел</a>
        <div class="modul-card__bar">
            <!-- Чтобы изменить процент заполнения шкалы - измените style="width: %" на нужное значение -->
            <div class="modul-card__bar-completed" style="width: 22%"></div>
        </div>
        <div class="modul-card__score-container">
            <span class="modul-card__required">Обязательный</span>
            <span class="modul-card__score">+5 баллов</span>
        </div>
    </div>

    <div class="modul-card">
        <h2 class="modul-card__title">Техники продаж</h2>
        <!-- Чтобы поставить галочку, вместо числа с процентом - удалите число внутри спана и добавьте класс "modul-card__percent--completed" -->
        <span class="modul-card__percent">56%</span>
        <p class="modul-card__passed-users">
            Прошли модуль: 957
            <i class="modul-card__passed-icon"></i>
        </p>
        <a href="#" class="modul-card__link">Перейти в раздел</a>
        <div class="modul-card__bar">
            <!-- Чтобы изменить процент заполнения шкалы - измените style="width: %" на нужное значение -->
            <div class="modul-card__bar-completed" style="width: 56%"></div>
        </div>
        <div class="modul-card__score-container">
            <span class="modul-card__required">Обязательный</span>
            <span class="modul-card__score">+5 баллов</span>
        </div>
    </div>

    <div class="modul-card">
        <h2 class="modul-card__title">Работа на корнере</h2>
        <!-- Чтобы поставить галочку, вместо числа с процентом - удалите число внутри спана и добавьте класс "modul-card__percent--completed" -->
        <span class="modul-card__percent">80%</span>
        <p class="modul-card__passed-users">
            Прошли модуль: 957
            <i class="modul-card__passed-icon"></i>
        </p>
        <a href="#" class="modul-card__link">Перейти в раздел</a>
        <div class="modul-card__bar">
            <!-- Чтобы изменить процент заполнения шкалы - измените style="width: %" на нужное значение -->
            <div class="modul-card__bar-completed" style="width: 80%"></div>
        </div>
        <div class="modul-card__score-container">
            <span class="modul-card__required">Обязательный</span>
            <span class="modul-card__score">+5 баллов</span>
        </div>
    </div>

    <div class="modul-card modul-card--exam">
        <h2 class="modul-card__title">Экзамен</h2>
        <!-- Чтобы поставить галочку, вместо числа с процентом - удалите число внутри спана и добавьте класс "modul-card__percent--completed" -->
        <span class="modul-card__percent">68%</span>
        <p class="modul-card__passed-users modul-card__passed-users--exam">
            Прошли модуль: 957
            <i class="modul-card__passed-icon modul-card__passed-icon--exam"></i>
        </p>
        <a href="#" class="modul-card__link modul-card__link--exam">Перейти в раздел</a>
        <div class="modul-card__bar">
            <!-- Чтобы изменить процент заполнения шкалы - измените style="width: %" на нужное значение -->
            <div class="modul-card__bar-completed" style="width: 68%"></div>
        </div>
        <div class="modul-card__score-container">
            <span class="modul-card__required">Обязательный</span>
            <span class="modul-card__score modul-card__score--exam">+55 баллов</span>
        </div>
    </div>

    <div class="modul-card">
        <h2 class="modul-card__title">Психотипы клиентов</h2>
        <!-- Чтобы поставить галочку, вместо числа с процентом - удалите число внутри спана и добавьте класс "modul-card__percent--completed" -->
        <span class="modul-card__percent">15%</span>
        <p class="modul-card__passed-users">
            Прошли модуль: 957
            <i class="modul-card__passed-icon"></i>
        </p>
        <a href="#" class="modul-card__link">Перейти в раздел</a>
        <div class="modul-card__bar">
            <!-- Чтобы изменить процент заполнения шкалы - измените style="width: %" на нужное значение -->
            <div class="modul-card__bar-completed" style="width: 15%"></div>
        </div>
        <div class="modul-card__score-container">
            <span class="modul-card__required">По желанию</span>
            <span class="modul-card__score">+5 баллов</span>
        </div>
    </div>
</section>
