<?php
/**
 * @var $seller User
 */

use \yii\widgets\ActiveForm;
use app\models\User;

?>

    <section class="account">
        <h1 class="account__title">Управление аккаунтами</h1>
        <h2 class="account__title2">Добавление нового продавца</h2>
        <button class="account__shevron" aria-label="скрыть форму добавления продавца"></button>
        <div class="account__form-container">

            <?php $form = ActiveForm::begin([
                'action' => '/cabinet/create-user',
                'options' => ['enctype' => 'multipart/form-data', 'class' => 'account__form']
            ]); ?>

            <?= $form->field(new User(), 'email')->textInput(['autocomplete' => 'off', 'placeholder' => 'Email'])->label(false) ?>
            <?= $form->field(new User(), 'first_name')->textInput(['autocomplete' => 'off', 'placeholder' => 'Имя'])->label(false) ?>
            <?= $form->field(new User(), 'last_name')->textInput(['autocomplete' => 'off', 'placeholder' => 'Фамилия'])->label(false) ?>
            <?= $form->field(new User(), 'password')->textInput(['autocomplete' => 'off', 'placeholder' => 'Пароль'])->label(false) ?>
            <?= $form->field(new User(), 'phone')->widget(\yii\widgets\MaskedInput::class, [
                'mask' => '+7 (999) 999-99-99',
                'options' => [
                    'class' => 'form-control placeholder-style',
                    'id' => 'phone2',
                    'placeholder' => ('Телефон')
                ],
                'clientOptions' => [
                    'clearIncomplete' => true
                ]
            ])->label(false) ?>

            <?= $form->field(new User(), 'city')->textInput(['autocomplete' => 'off', 'placeholder' => 'Город'])->label(false) ?>
            <h2 class="account__title2 account__title2--wide">Аватар</h2>
            <div class="account__ava-container" id="drop-area">
                <div class="account__ava-form">
                    <p class="account__ava-text">Перетащите фотографию в эту область или кликните, чтобы загрузить
                        вручную</p>
                    <label for="fileElem" class="account__ava-label"></label>
                    <input type="file" id="fileElem" accept="image/*" onchange="handleFiles(this.files)">
                </div>
                <div class="account__ava-preview" id="avaPreview"></div>
                <progress id="progress-bar" max=100 value=0></progress>
            </div>
            <div style="text-align: center; width: 100%">
                <?= \yii\helpers\Html::submitButton(Yii::t('app', 'Добавить'), ['class' => 'account__add-button blue-border-button']) ?>
            </div>
            <?php ActiveForm::end(); ?>

        </div>
    </section>
<?php
\yii\widgets\Pjax::begin();
?>
    <section class="sellers">
        <h2 class="sellers__title">Список продавцов</h2>
        <div class="sellers__sort">
            <span class="sellers__sort-text">Сортировать по</span>
            <a href="?sort=first_name&sort_type=<?= $_GET['sort_type'] == 'asc' ? 'desc' : 'asc' ?>"
               class="sellers__sort-alph sellers__sort-active" style="font-weight: 100">алфавиту</a>
            <a href="?sort=date_registration&sort_type=<?= $_GET['sort_type'] == 'asc' ? 'desc' : 'asc' ?>"
               class="sellers__sort-date" style="font-weight: 100">дате добавления</a>
            <a href="?sort=first_name&sort_type=<?= $_GET['sort_type'] == 'asc' ? 'desc' : 'asc' ?>"
               class="sellers__sort-default" style="font-weight: 100">умолчанию</a>
            <div class="sellers__search">
                <button class="sellers__search-button"></button>
                <?php $form = ActiveForm::begin([
                    'action' => '/cabinet/create-user',
                    'options' => ['enctype' => 'multipart/form-data', 'class' => 'account__form', 'style' => 'margin-bottom:0']
                ]); ?>
                <input type="text" placeholder="начните вводить имя" aria-label="поиск продавца по имени"
                       style="margin-bottom: 0">
                <?php ActiveForm::end(); ?>
            </div>
        </div>
        <?php
        foreach (Yii::$app->user->identity->sellers as $seller) {
            ?>
            <div class="sellers__card">
                <header class="sellers__card-header">
                    <img url="" alt="">
                    <span class="sellers__card-name"><?= $seller->getFIO() ?></span>
                    <a class="sellers__card-email">
                        <span class="sellers__card-email-text"><?= $seller->email ?></span>
                        <span class="sellers__card-email-icon"></span>
                    </a>
                    <button class="sellers__card-edit">Редактировать</button>
                    <button class="sellers__card-del">Удалить</button>
                </header>
                <div class="sellers__card-table-container">
                    <table class="sellers__card-table">
                        <tr>
                            <th>Название теста</th>
                            <th>Баллов</th>
                            <th>Попыток</th>
                            <th>Ответы</th>
                            <th>Действия</th>
                        </tr>
                        <tr>
                            <td>О бренде</td>
                            <td>78</td>
                            <td>2</td>
                            <td>70/100</td>
                            <td>50%</td>
                        </tr>
                        <tr>
                            <td>Уход за кожей</td>
                            <td>99</td>
                            <td>3</td>
                            <td>100/100</td>
                            <td>100%</td>
                        </tr>
                    </table>
                </div>
            </div>
            <?php
        }
        ?>
    </section>
<?php
\yii\widgets\Pjax::end();

