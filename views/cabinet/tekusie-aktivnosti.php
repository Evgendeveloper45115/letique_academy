<?php
/**
 * @var $models \app\models\Content[]
 */
$menu = \app\models\Menu::getCategory();
$parent = null;
$children = [];

?>
<section class="activity">
    <h1 class="activity__title"><?= $menu->name ?></h1>
    <?php
    if (!empty($models)) {
        ?>
        <div class="activity__container">
            <?php
            foreach ($models as $model) {
                ?>
                <a href="/cabinet/<?= $_GET['category'] ?>/<?= $model->id ?>" class="activity__card">
                    <img src="/uploads/<?= $model->menu->id ?>/<?= $model->image ?>"
                         alt="<?= $model->name ?>"
                         class="activity__img">
                    <h2 class="activity__card-title"><?= $model->name ?></h2>
                    <?php
                    if (strtotime($model->date_to) < time()) {
                        ?>
                        <p class="activity__date">
                            Завершен
                        </p>
                        <?php
                    } else {
                        ?>
                        <p class="activity__date">
                            до <?= date('d', strtotime($model->date_to)) . ' ' . \app\models\Content::getMonths($model->date_to) . ' ' . date('Y', strtotime($model->date_to)) ?>
                        </p>
                        <?php
                    }
                    ?>
                    <span class="activity__location"><?= $model->location ?></span>
                </a>
                <?php
            }
            ?>
        </div>
        <?php
    }
    ?>
</section>
