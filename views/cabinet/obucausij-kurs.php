<?php
/**
 * @var $models \app\models\Content[]
 */
$menu = \app\models\Menu::getCategory();
$parent = null;
$children = [];
if ($menu && !isset($_GET['sub_category'])) {
    if ($menu == null) {
        $parent = $menu->parents(1)->one();
    }
}
?>
    <section class="courses">
        <h1 class="courses__title"><?= $parent ? $parent->name : (is_array($menu) ? $menu[0]->name : $menu->name) ?></h1>

        <ul class="courses__inner-nav-list courses__inner-nav-list--material courses__inner-nav-list--parent">
            <?php
            if (!is_array($menu)) {
                foreach ($menu->children(1)->all() as $item) {
                    ?>
                    <li class="courses__inner-item">
                        <a href="/cabinet/<?= $_GET['category'] ?>/<?= $_GET['sub_category'] ?>/<?= $item->url ?>"
                           class="courses__inner-link courses__inner-link<?= $item->url == $_GET['sub_category'] || $item->url == $_GET['children'] ? ' courses__inner-link--current' : null ?>"><?= preg_replace('/(.*)\-\d{1,2}/', '$1', $item->name) ?></a>
                    </li>
                    <?php

                }
            } else {
                foreach ($menu[0]->children(1)->all() as $item) {
                    ?>
                    <li class="courses__inner-item">
                        <a href="/cabinet/<?= $_GET['category'] ?>/<?= $item->url ?>"
                           class="courses__inner-link courses__inner-link<?= $item->url == $_GET['sub_category'] || $item->url == $_GET['children'] ? ' courses__inner-link--current' : null ?>"><?= preg_replace('/(.*)\-\d{1,2}/', '$1', $item->name) ?></a>
                    </li>
                    <?php

                }
            }
            ?>
        </ul>
        <?php
        $menu_sec = \app\models\Menu::find()->where(['url' => $_GET['children']])->one();
        if ($menu_sec && $menu_sec->children(1)->all()) {
            ?>
            <ul class="courses__inner-nav-list courses__inner-nav-list--material courses__inner-nav-list--children">
                <?php
                foreach ($menu_sec->children(1)->all() as $child) {
                    ?>
                    <?php
                    if (isset($_GET['child'])) {
                        ?>
                        <li class="courses__lesson-item">
                            <a href="/cabinet/<?= $_GET['category'] ?>/<?= $_GET['sub_category'] ?>/<?= $_GET['children'] ?>/<?= $child->url ?>"
                               class="courses__lesson-link<?= $child->url == $_GET['child'] ? '--current' : null ?>"><?= preg_replace('/(.*)\-\d{1,2}/', '$1', $child->name);
                                ?></a>
                        </li>
                        <?php
                    } else {
                        ?>
                        <li class="courses__lesson-item">
                            <a href="/cabinet/<?= $_GET['category'] ?>/<?= $_GET['sub_category'] ?>/<?= $child->url ?>"
                               class="courses__lesson-link<?= $child->url == $_GET['children'] ? '--current' : null ?>"><?= preg_replace('/(.*)\-\d{1,2}/', '$1', $child->name) ?></a>
                        </li>
                        <?php
                    }
                    ?>
                    <?php
                }
                ?>
            </ul>
            <?php
        }
        ?>

        <ul class="courses__progress-list courses__progress-list--lesson">
            <li class="courses__progress-item courses__progress-item--completed"></li>
            <li class="courses__progress-item courses__progress-item--completed"></li>
            <li class="courses__progress-item"></li>
            <li class="courses__progress-item"></li>
            <li class="courses__progress-item"></li>
            <li class="courses__progress-item"></li>
            <li class="courses__progress-text">Готово!</li>
        </ul>
    </section>

<?php
foreach ($models as $model) {
    ?>
    <section class="video-lesson">
        <h2 class="video-lesson__title"><?= $model->name ?></h2>
        <?php
        if ($model->video) {
            ?>
            <div class="video-lesson__video">
                <div class="video">
                    <a class="video__link" href="https://youtu.be/<?= $model->video ?>">
                        <picture>
                            <source srcset="https://i.ytimg.com/vi_webp/<?= $model->video ?>/maxresdefault.webp"
                                    type="image/webp">
                            <img class="video__media"
                                 src="https://i.ytimg.com/vi/<?= $model->video ?>/maxresdefault.jpg"
                                 alt="Проверка программы для автоматического построения в курсовом проекте по ТММ">
                        </picture>
                    </a>
                    <button class="video__button" aria-label="Запустить видео">
                        <svg width="68" height="48" viewBox="0 0 68 48">
                            <path class="video__button-shape"
                                  d="M66.52,7.74c-0.78-2.93-2.49-5.41-5.42-6.19C55.79,.13,34,0,34,0S12.21,.13,6.9,1.55 C3.97,2.33,2.27,4.81,1.48,7.74C0.06,13.05,0,24,0,24s0.06,10.95,1.48,16.26c0.78,2.93,2.49,5.41,5.42,6.19 C12.21,47.87,34,48,34,48s21.79-0.13,27.1-1.55c2.93-0.78,4.64-3.26,5.42-6.19C67.94,34.95,68,24,68,24S67.94,13.05,66.52,7.74z"></path>
                            <path class="video__button-icon" d="M 45,24 27,14 27,34"></path>
                        </svg>
                    </button>
                </div>
            </div>
            <?php
        }
        if ($model->image) {
            ?>
            <div class="download-doc__ext --card-high js-high download-doc__ext--card-high">
                <img src="/uploads/<?= $model->menu->id ?>/<?= $model->image ?>" alt=""
                     class="download-doc__ext-img">
            </div>
            <?php
        }
        if ($model->description) {
            echo $model->description;
        }
        ?>
    </section>
    <?php
    if ($model->images) {
        ?>
        <section class="download-doc">
            <h2 class="download-doc__title">Скачайте документы:</h2>
            <ul class="download-doc__list">
                <?php
                foreach ($model->images as $image) {
                    ?>
                    <li class="download-doc__item">
                        <div class="download-doc__ext">
                            <span class="download-doc__ext-text"><?= pathinfo($image->name)['extension'] ?></span>
                        </div>
                        <div>
                            <h3 class="download-doc__item-title"><?= $image->title ?></h3>
                            <a href="/uploads/<?= $model->menu->id ?>/<?= $image->name ?>"
                               class="download-doc__link"
                               download="<?= $model->name ?>">Скачать
                                документ</a>
                        </div>
                    </li>
                    <?php
                }
                ?>
            </ul>
        </section>
        <?php
    }
    ?>

    <?php
}
$menu = \app\models\Menu::getLastCategory();
if ($menu) {
    $next = $menu->next()->one();
    ?>
    <section class="mark-task">
        <a href="#" class="mark-task__button blue-border-button">
            <span class="mark-task__button-text1">Пометить задание как выполненное</span>
            <span class="mark-task__button-text2">Выполнено!</span>
        </a>
        <ul class="mark-task__nav">
            <?php
            if ($menu) {
                if (isset($_GET['child'])) {
                    ?>
                    <li class="mark-task__current">
                        <a href="/cabinet/<?= $_GET['category'] ?>/<?= $_GET['sub_category'] ?>/<?= $_GET['children'] ?>/<?= $menu->url ?>"
                           class="mark-task__nav-link"><?= $menu->name ?></a>
                    </li>
                    <?php
                } else {
                    ?>
                    <li class="mark-task__current">
                        <a href="/cabinet/<?= $_GET['category'] ?>/<?= $_GET['sub_category'] ?>/<?= $menu->url ?>"
                           class="mark-task__nav-link"><?= $menu->name ?></a>
                    </li>
                    <?php
                }
            }
            if ($next) {
                if (isset($_GET['child'])) {
                    ?>
                    <li class="mark-task__next">
                        <a href="/cabinet/<?= $_GET['category'] ?>/<?= $_GET['sub_category'] ?>/<?= $_GET['children'] ?>/<?= $next->url ?>"
                           class="mark-task__nav-link"><?= $next->name ?></a>
                    </li>
                    <?php
                } else {
                    ?>
                    <li class="mark-task__next">
                        <a href="/cabinet/<?= $_GET['category'] ?>/<?= $_GET['sub_category'] ?>/<?= $next->url ?>"
                           class="mark-task__nav-link"><?= $next->name ?></a>
                    </li>
                    <?php
                }

            }
            ?>
        </ul>
    </section>
    <?php
}
