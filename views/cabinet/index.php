<?php
/* @var $this yii\web\View */
$this->title = 'Обучающий курс Letique';
?>
<section class="intro">
    <h1 class="intro__title">Обучающий<br>курс Letique</h1>
    <a href="/cabinet/obucausij-kurs" class="intro__link">Начать обучение</a>
    <img class="intro__img" src="/images/main-bg.png" alt="Девушка с ноутбуком" width="975" height="732">
</section>
