<?php
$percent = end(json_decode($passed)) / $testAll * 100;
?>
<main class="main--test"> <!-- внимание, здесь на main класс который необходим для вёрстки -->


    <!-- Секция с поздравлениями об окончинии, сейчас скрыта -->
    <section class="test"><!-- скрыта здесь с помощью style="display: none;" -->
        <h1 class="test__title"><?= $model->name ?></h1>
        <p class="test__congrat">Поздравляем! Тест пройден!</p>
        <p class="test__of"><?= end(json_decode($passed)) ?>/<?= $testAll ?></p>
        <p class="test__you-are-ready">Вы готовы к работе</p>
        <a href="" class="blue-border-button test__another">Пройти другой тест</a>
    </section>

    <footer class="t-footer">
        <div class="t-footer__progress-container">
            <div class="t-footer__progress-under-bar">
                <div class="t-footer__progress-bar"
                     style="width: <?= $percent ?>%;"></div>
                <!-- Здесь меняются проценты, сейчас 25% стоит -->
            </div>
            <div class="t-footer__progress__circle <?= $percent == 100 ? 'active' : null ?>"></div>
            <!-- Здесь круглику добавляем класс active и он станет синий -->
        </div>
        <span class="t-footer__of"><?= end(json_decode($passed)) ?>/<?= $testAll ?></span>
        <a href="#" class="t-footer__link">Выйти</a>
    </footer>

    <!-- Картинка девушки -->
    <picture>
        <source media="(max-width: 750px)"
                srcset="/images/test-girl-mobile.png 1x, /images/test-girl-mobile@2x.png 2x, /images/test-girl-mobile@3x.png 3x">
        <img class="test__img"
             srcset="/images/test-girl@2x.png 2x, /images/test-girl@3x.png 3x"
             src="/images/test-girl.png"
             alt="Девушка со смартфоном"
        >
    </picture>

</main>

