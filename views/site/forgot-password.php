<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

/* @var $model app\models\forms\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Авторизация';
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="login login-in-complete">
    <h1 class="login__title">Восстановление пароля</h1>
    <?php $form = ActiveForm::begin([
        'id' => 'login-form',
        'validateOnChange' => true,
        'enableClientValidation' => true,

        'options' => ['class' => 'login__form account__form login__form--left', 'name' => 'sign_in'],
        'fieldConfig' => [
            'options' => ['class' => 'form_group clearfix'],
            'template' => "{label}\n<div class=\"\">{input}\n<div class=\"error\">{error}</div></div>",
        ],
    ]); ?>

    <?= $form->field($model, 'email')->textInput(['class' => 'sing_input', 'placeholder' => 'Введите почту', 'required' => 'required'])->label(false) ?>
    <?php
    if (isset($error) && $error != null) {
        ?>
        <div class="error"><?= $error ?></div>
        <?php
    }
    if (isset($success) && $success != null) {
        ?>
        <div class="error"><?= $success ?></div>
        <?php
    }
    ?>

    <a href="" class="login__form-forgot-link login__form-forgot-link--password">Отправить еще раз</a>
    <div class="login__form-btn-container login__form-btn-container--left">
        <?= Html::button('Отправить', ['type' => 'submit', 'class' => 'blue-border-button blue-border-button--own-size email_send']) ?>
    </div>
    <?php ActiveForm::end(); ?>
    <img srcset="/images/login@2x.png 2x, /images/login@3x.png 3x"
         alt="Девушка во смартфоном" class="login__img"
         src="/images/login.png">
</section>
<section class="login login-complete" style="display: none;"> <!-- скрыта здесь с помощью style="display: none;" -->
    <h1 class="login__title">Восстановление пароля</h1>
    <p class="login__text">Ваш новый пароль отправлен на указанную почту</p>
    <div class="login__form-btn-container login__form-btn-container--left login__form-btn-container--mb">
        <a href="/" class="blue-border-button blue-border-button--own-size">Войти</a>
    </div>
    <img srcset="/images/login@2x.png 2x, /images/login@3x.png 3x"
         alt="Девушка во смартфоном" class="login__img"
         src="/images/login.png">
</section>