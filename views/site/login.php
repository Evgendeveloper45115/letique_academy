<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Авторизация';
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="login">
    <div>
        <h1 class="login__title"><?= $this->title ?></h1>
    </div>
    <?php $form = ActiveForm::begin([
        'id' => 'login-form',
        'validateOnChange' => false,
        'enableClientValidation' => true,

        'options' => ['class' => 'login__form account__form', 'name' => 'sign_in'],
        'fieldConfig' => [
            'options' => ['class' => 'form_group clearfix'],
            'template' => "{label}\n<div class=\"\">{input}\n<div class=\"error\">{error}</div></div>",
        ],
    ]); ?>

    <?= $form->field($model, 'email')->textInput(['class' => 'sing_input', 'placeholder' => 'Логин'])->label(false) ?>

    <?= $form->field($model, 'password')->passwordInput(['class' => 'sing_input', 'placeholder' => 'Пароль'])->label(false) ?>

    <a href="/site/forgot-password" class="login__form-forgot-link">Забыли пароль?</a>
    <div class="login__form-btn-container">
        <button class="blue-border-button">Войти</button>
    </div>

    <?php ActiveForm::end(); ?>
    <img srcset="/images/login@2x.png 2x, /images/login@3x.png 3x" alt="Девушка со смартфоном" class="login__img"
         src="/images/login.png">
</section>