<?php
$root = \app\models\Menu::find()->roots()->one();
?>
<div class="mob-modal" id="mob-modal">
    <button class="mob-modal__btn" id="mob-modal__close" aria-label="Свернуть меню"></button>
    <ul class="mob-modal__list">
        <?php
        foreach ($root->children(1)->all() as $cat) {
            ?>
            <li class="mob-modal__item">
                <!-- Если внутри li есть список показываемый при нажатии, то ставим доп. класс -->
                <a href="/cabinet/<?= $cat->url ?>" class="mob-modal__link"><?= $cat->name ?></a>
                <?php
                ?>
                <?php
                if ($cat->children(1)->all()) {
                    ?>
                    <ul class="mob-modal__list2">
                        <?php
                        foreach ($cat->children(1)->all() as $cat_1) {
                            ?>
                            <li class="mob-modal__item2">
                                <a href="/cabinet/<?= $cat->url . '/' . $cat_1->url ?>"
                                   class="mob-modal__link2"><?= $cat_1['name'] ?></a>
                            </li>
                            <?php
                        }

                        ?>
                    </ul>
                    <?php
                }
                ?>
            </li>
            <?php
        }

        ?>
    </ul>
    <!-- Отображение до входа в аккаунт -->
    <div class="mob-modal__before-enter" style="display: none;">
        <a href="" class="mob-modal__before-link">Войти в аккаунт</a>
    </div>

    <!-- Отображение этого дива вместо предыдущего при входе -->
    <div class="mob-modal__after-enter">
        <p class="mob-modal__name">Елена прекрасная</p>
        <a href="" class="mob-modal__after-link">Профиль</a>
        <a href="" class="mob-modal__after-link">Вход</a>
        <span class="mob-modal__bonuses">2500</span>
    </div>

    <div class="mob-modal__lang-container">
        <span class="mob-modal__lang-text">Выбрать язык: </span>
        <select class="header__lang" aria-label="выбор языка">
            <option value="ru" selected>Ru</option>
            <option value="en">En</option>
        </select>
    </div>
</div>
