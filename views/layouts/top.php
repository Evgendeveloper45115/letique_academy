<?php
$root = \app\models\Menu::find()->roots()->one();
$contentDesign = \app\models\Content::find()->where(['cat' => 'category/' . $_GET['category']])->one();
?>
<nav class="main-nav">
    <a href="/"> <img class="main-nav__logo logo" src="/images/logo.svg" alt="Логотип"></a>

    <ul class="main-nav__list">
        <?php
        foreach ($root->children(1)->all() as $cat) {
            ?>
            <li class="main-nav__item">
                <a href="/cabinet/<?= $cat->url ?>"
                   class="main-nav__link<?= $cat['url'] == $_GET['category'] ? '--current' : null ?>"><?= $cat['name'] ?></a>
            </li>
            <?php
            if ($_GET['category'] == $cat['url']) {
                ?>
                <?php
                if ($cat->children(1)->all()) {
                    ?>
                    <ul class="main-nav__list-2">
                        <?php
                        foreach ($cat->children(1)->all() as $cat_1) {

                            ?>
                            <li class="main-nav__item-2">
                                <a href="/cabinet/<?= $cat->url . '/' . $cat_1->url ?>"
                                   class="main-nav__link<?= $cat_1['url'] == $_GET['sub_category'] ? '--current' : null ?>"><?= $cat_1['name'] ?></a>
                            </li>
                            <?php
                        }

                        ?>
                    </ul>
                    <?php
                }
                ?>
                <?php
            }
        }

        ?>
    </ul>
</nav>
