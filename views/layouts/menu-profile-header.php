<button class="main-content__button"></button>
<button class="mobile-menu-open" id="mobile-menu-open"></button>
<header class="header">
    <div class="header__menu header__menu--inner">
        <div class="header__lang-container">
            <select class="header__lang" aria-label="выбор языка">
                <option value="ru" selected>Ru</option>
                <option value="en">En</option>
            </select>
        </div>
        <a class="header__bell header__bell--allert"></a>
        <div class="header__bonuses">2 500</div>
        <label class="header__username-block">
            <span class="header__username">Елена прекрасная</span>
            <button class="header__button" id="header__button"></button>
            <div class="header__username-menu display-none">
                <a href="" class="header__username-menu-link">Профиль</a>
                <a href="/site/logout" class="header__username-menu-link">Выход</a>
            </div>
        </label>
    </div>
</header>
