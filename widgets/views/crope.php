<?php
/**
 * @var \yii\db\ActiveRecord $model
 * @var \budyaga\cropper\Widget $widget
 *
 */

use yii\helpers\Html;

?>

<div class="cropper-widget">
    <?= Html::activeHiddenInput($model, $widget->attribute, ['class' => 'photo-field']); ?>
    <?= Html::hiddenInput('width', $widget->width, ['class' => 'width-input']); ?>
    <?= Html::hiddenInput('height', $widget->height, ['class' => 'height-input']); ?>
    <?= Html::img(
        $model->{$widget->attribute} != ''
            ? $model->{$widget->attribute}
            : $widget->noPhotoImage,
        [
            'style' => 'max-height: ' . $widget->thumbnailHeight . 'px; max-width: ' . $widget->thumbnailWidth . 'px',
            'class' => 'thumbnail',
            'data-no-photo' => $widget->noPhotoImage
        ]
    ); ?>
    <div class="new-photo-area"
         style="height: <?= $widget->cropAreaHeight; ?>px; width: <?= $widget->cropAreaWidth; ?>px;">
        <div class="cropper-label">
            <span>Нажмите или перетащите фото в эту область</span>
        </div>
    </div>
    <div class="cropper-buttons">
        <button type="button" class="btn btn-sm btn-success crop-photo hidden" style="background-color: #2b343c;"
                aria-label="<?= Yii::t('cropper', 'CROP_PHOTO'); ?>">
            <span class="glyphicon glyphicon-scissors" aria-hidden="true"></span> Обрезать фото
        </button>
        <button type="button" class="btn btn-sm btn-info upload-new-photo hidden"
                style="<?= Yii::$app->controller->action->id == 'profile' ? 'margin-top: 10px;' : null ?>background-color: #2b343c;"
                aria-label="Загрузить фото до 2мб">
            <span class="glyphicon glyphicon-picture" aria-hidden="true"></span> Загрузить другое фото.
        </button>
    </div>
    <div class="progress hidden" style="width: <?= $widget->cropAreaWidth; ?>px;">
        <div class="progress-bar progress-bar-striped progress-bar-success active" role="progressbar" style="width: 0%">
            <span class="sr-only"></span>
        </div>
    </div>
</div>
