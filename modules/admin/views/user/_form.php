<?php

use yii\helpers\Html;
use kartik\form\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */
?>
<?php
?>
<div class="user-form">
    <?php
    $form = ActiveForm::begin([
        'id' => 'login-form-horisonal',
        'type' => ActiveForm::TYPE_VERTICAL,
        'options' => ['enctype' => 'multipart/form-data']
    ]);
    ?>
    <div class="col-row row">
        <div class="col-md-6">
            <?= $form->field($model, 'email')->textInput(['autocomplete' => 'off']) ?>
            <?= $form->field($model, 'first_name')->textInput(['autocomplete' => 'off']) ?>
            <?= $form->field($model, 'last_name')->textInput(['autocomplete' => 'off']) ?>
            <?= $form->field($model, 'password')->textInput(['autocomplete' => 'off']) ?>
            <?= $form->field($model, 'phone')->widget(\yii\widgets\MaskedInput::className(), [
                'mask' => '+7 (999) 999-99-99',
                'options' => [
                    'class' => 'form-control placeholder-style',
                    'id' => 'phone2',
                    'placeholder' => ('Телефон')
                ],
                'clientOptions' => [
                    'clearIncomplete' => true
                ]
            ]) ?>

        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'role')->dropDownList(\app\models\User::$roles) ?>
            <?= $form->field($model, 'city')->textInput(['autocomplete' => 'off']) ?>
            Аватар
            <?= \kartik\file\FileInput::widget([
                'model' => $model,
                'language' => 'ru',
                'attribute' => 'img',
                'options' => [
                    'multiple' => false
                ],

                'pluginOptions' => [
                    'initialPreview' => $model->avatar ? '/uploads/users/' . $model->avatar : false,
                    'initialPreviewConfig' => [[
                        'key' => $model->id
                    ]],
                    'initialPreviewAsData' => true,
                    'deleteUrl' => \yii\helpers\Url::toRoute(['delete-image-avatar']),
                    'overwriteInitial' => false,
                ]]) ?>

        </div>
        <div class="col-md-12">
            <div class="form-group">
                <?= Html::submitButton(Yii::t('app', 'Сохранить'), ['class' => 'btn btn-success']) ?>
            </div>
            <div class="form-group">
                <?= Html::a(Yii::t('app', 'Назад'), Yii::$app->request->referrer, ['class' => 'btn btn-success']) ?>
            </div>
        </div>
        <div class="col-md-6"></div>

    </div>

    <?php ActiveForm::end(); ?>

</div>
