<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model \app\models\GroupMember */

$this->title = $model->user->email;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="user-view">

    <p>
        <?= Html::a(Yii::t('app', 'Редактировать'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Удалить'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>
    <?php
    echo \kartik\rating\StarRating::widget([
        'name' => 'rating',
        'value' => $model->rating,
        'pluginOptions' =>
            [
                'showClear' => true,
                'stars' => 5,
                'min' => 0,
                'max' => 5,
                'step' => 1,
                'starCaptions' => new \yii\web\JsExpression("function(val){
                     return  val;
                }"),
            ]
    ]);
    ?>
    <?= Html::dropDownList('cat', $model->vote_category, ['' => ''] + $model::$cats, ['class' => 'cat_vote', 'style' => 'margin-bottom:10px']) ?>
    <?= Html::hiddenInput('member_id', $model->id, ['class' => 'cat_vote_id']) ?>
    <?php $form = \kartik\form\ActiveForm::begin([
        'options' => ['enctype' => 'multipart/form-data']
    ]); ?>


    <?= \kartik\file\FileInput::widget([
        'model' => $model,
        'language' => 'ru',
        'attribute' => 'img',
        'pluginOptions' => \app\helpers\My::getImage($model, $model->avatar, 'profile', 'delete-image')
    ]) ?>
    <br>
    <br>
    <br>
    <?php \kartik\form\ActiveForm::end(); ?>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'group.num',
            'user.email',
            'phone',
            'first_name',
            'last_name',
            'patronymic',
            [
                'attribute' => 'reportHasUser.user_text',
                'label' => 'Отчет',
                'format' => 'raw',
                'value' => function (\app\models\GroupMember $model) {
                    return '----' . implode('---<br>----', \yii\helpers\ArrayHelper::map($model->reportHasUsers, 'report.week', function (\app\models\ReportHasUser $model) {
                            return 'Неделя - ' . $model->report->week . ' - ' . $model->user_text;
                        }));
                }
            ],
            [
                'attribute' => 'progress.weight',
                'label' => 'Вес',
                'format' => 'raw',
                'value' => function (\app\models\GroupMember $model) {
                    return '----' . implode('---<br>----', \yii\helpers\ArrayHelper::map($model->progressRel, 'week', function (\app\models\Progress $model) {
                            return 'Неделя - ' . $model->week . ' - ' . $model->weight . ' кг';
                        }));
                }
            ],
            [
                'attribute' => 'progress.bust',
                'label' => 'Обьем груди',
                'format' => 'raw',
                'value' => function (\app\models\GroupMember $model) {
                    return '----' . implode('---<br>----', \yii\helpers\ArrayHelper::map($model->progressRel, 'week', function (\app\models\Progress $model) {
                            return 'Неделя - ' . $model->week . ' - ' . $model->bust . ' см';
                        }));
                }
            ],
            [
                'attribute' => 'progress.waist',
                'label' => 'Обьем талии',
                'format' => 'raw',
                'value' => function (\app\models\GroupMember $model) {
                    return '----' . implode('---<br>----', \yii\helpers\ArrayHelper::map($model->progressRel, 'week', function (\app\models\Progress $model) {
                            return 'Неделя - ' . $model->week . ' - ' . $model->waist . ' см';
                        }));
                }
            ],
            [
                'attribute' => 'progress.hip',
                'label' => 'Обьем бедер',
                'format' => 'raw',
                'value' => function (\app\models\GroupMember $model) {
                    return '----' . implode('---<br>----', \yii\helpers\ArrayHelper::map($model->progressRel, 'week', function (\app\models\Progress $model) {
                            return 'Неделя - ' . $model->week . ' - ' . $model->hip . ' см';
                        }));
                }
            ],
            [
                'attribute' => 'progress.butt',
                'label' => 'Обьем ягодиц',
                'format' => 'raw',
                'value' => function (\app\models\GroupMember $model) {
                    return '----' . implode('---<br>----', \yii\helpers\ArrayHelper::map($model->progressRel, 'week', function (\app\models\Progress $model) {
                            return 'Неделя - ' . $model->week . ' - ' . $model->butt . ' см';
                        }));
                }
            ],
            [
                'attribute' => '',
                'label' => 'Спереди',
                'format' => 'raw',
                'value' => function (\app\models\GroupMember $model) {
                    $arr = \yii\helpers\ArrayHelper::index($model->progressRel, 'week');
                    $str = '';
                    foreach ($arr as $progress) {
                        if ($progress->photo) {
                            $str .= Html::img('/uploads/progress/' . $progress->photo, ['style' => 'width:25%']);
                        }
                    }
                    return $str;
                }
            ],
            [
                'attribute' => '',
                'label' => 'Сбоку',
                'format' => 'raw',
                'value' => function (\app\models\GroupMember $model) {
                    $arr = \yii\helpers\ArrayHelper::index($model->progressRel, 'week');
                    $str = '';
                    foreach ($arr as $progress) {
                        if ($progress->photo) {
                            $str .= Html::img('/uploads/progress/' . $progress->photo_second, ['style' => 'width:25%']);
                        }
                    }
                    return $str;
                }
            ],
            [
                'attribute' => '',
                'label' => 'Сзади',
                'format' => 'raw',
                'value' => function (\app\models\GroupMember $model) {
                    $arr = \yii\helpers\ArrayHelper::index($model->progressRel, 'week');
                    $str = '';
                    foreach ($arr as $progress) {
                        if ($progress->photo) {
                            $str .= Html::img('/uploads/progress/' . $progress->photo_third, ['style' => 'width:25%']);
                        }
                    }
                    return $str;
                }
            ],
        ]
    ]) ?>
</div>
