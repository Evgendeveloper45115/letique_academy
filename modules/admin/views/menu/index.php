<?php

use yii\helpers\Html;
use klisl\nestable\Nestable;
use yii\helpers\Url;

/* @var $query \yii\db\ActiveQuery */

$this->title = 'Меню сайта';
$this->params['breadcrumbs'][] = $this->title;
?>
<div id="nestable-menu">
    <button class="btn btn-default" type="button" data-action="expand-all">Развернуть</button>
    <button class="btn btn-default" type="button" data-action="collapse-all">Свернуть</button>
</div>

<div class="menu-index">

    <p><?= Html::a('Создать новый раздел', ['create'], ['class' => 'btn btn-default']) ?></p>

    <?= Nestable::widget([
        'type' => Nestable::TYPE_WITH_HANDLE,
        'query' => $query,
        'modelOptions' => [
            'name' => function ($model) {
                return $model->name . ' - ' . $model->url;
            }, //поле из БД с названием элемента (отображается в дереве)
        ],
        'pluginEvents' => [
            'change' => 'function(e) {}', //js событие при выборе элемента
        ],
        'pluginOptions' => [
            'maxDepth' => 10,//максимальное кол-во уровней вложенности

            'collapseAll' => 'collapseAll',
        ],
        'update' => Url::to(['menu/update']), //действие по обновлению
        'delete' => Url::to(['menu/delete']), //действие по удалению
        'viewItem' => Url::to(['menu/view']), //действие по удалению
    ]);
    ?>
</div>