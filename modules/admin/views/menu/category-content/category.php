<?php

use app\models\Content;
use app\models\Menu;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\User */
/* @var $dataProvider yii\data\ActiveDataProvider */
$cat = Content::menuSearch();
$this->title = $cat->name;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">
    <?= \kartik\grid\GridView::widget([
        'id' => 'kv-grid-demo',
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'design_id',
                'format' => 'raw',
                'value' => function (\app\models\Content $model) {
                    return \app\models\Content::$design_ids[$model->menu->design_id];
                },
                'filterInputOptions' => [
                    'class' => 'form-control',
                    'placeholder' => 'Email'
                ],

            ],
            [
                'attribute' => 'name',
                'format' => 'raw',
                'value' => function (\app\models\Content $model) {
                    return $model->name;
                },
                'filterInputOptions' => [
                    'class' => 'form-control',
                ],

            ],
            [
                'attribute' => 'image',
                'label' => 'Картинка карточки',
                'format' => 'raw',
                'value' => function (\app\models\Content $model) {
                    $url = Yii::$app->request->url;
                    $cat = end(explode('/', $url));
                    $menu = Menu::findOne(['url' => $cat]);
                    if ($model->image && is_file('uploads/' . $menu->id . '/' . $model->image)) {
                        return Html::img('/uploads/' . $menu->id . '/' . $model->image, ['style' => 'max-width:200px']);
                    }
                }
            ],
            [
                'attribute' => 'img',
                'label' => 'Документы',
                'format' => 'raw',
                'value' => function (\app\models\Content $model) {
                    if ($model->images) {
                        return implode('<br>', \yii\helpers\ArrayHelper::map($model->images, 'name', 'title'));
                    }
                }
            ],
            'date_create',
            'date_to',
            'location',
            [
                'attribute' => 'video',
                'value' => function (\app\models\Content $model) {
                    return $model->video;
                }
            ],
            [
                'attribute' => 'description',
                'format' => 'html',
                'value' => function (\app\models\Content $model) {
                    return \yii\helpers\StringHelper::truncate($model->description, 100);
                }
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '<span style="text-wrap: none">{update} {delete}</span>',
                'header' => 'Действия',
                'buttons' => [
                    'update' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-pencil custom"></span>', \yii\helpers\Url::to(['update/category/' . $_GET['category'] . '/' . $model->id]), [
                            'title' => 'Редактировать'
                        ]);

                    },
                    'delete' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', \yii\helpers\Url::to(['delete/category/' . $model->id]), [
                            'title' => 'Удалить',
                            'data' => [
                                'confirm' => 'Вы точно хотите удалить ' . $model->name . '?',
                                'method' => 'post',
                            ],
                        ]);

                    },
                ]
            ]
            ,
        ], // check the configuration for grid columns by clicking button above
        'containerOptions' => ['style' => 'overflow: auto'], // only set when $responsive = false
        'headerRowOptions' => ['class' => 'kartik-sheet-style'],
        'filterRowOptions' => ['class' => 'kartik-sheet-style'],
        'pjax' => true, // pjax is set to always true for this demo
        // set your toolbar
        'toolbar' => [
            [
                'content' => '<a href="' . \yii\helpers\Url::to(['create/category/' . \app\helpers\My::getUrl()]) . '" class="btn btn-default" title="Создать новую группу">Создать</a>',
                'options' => ['class' => 'btn-group mr-2']
            ],
            '{export}',
        ],
        'toggleDataContainer' => ['class' => 'btn-group mr-2'],
        // set export properties
        'export' => [
            'fontAwesome' => true
        ],
        // parameters from the demo form
        'bordered' => true,
        'striped' => true,
        'pjaxSettings' => [
            'options' => [
                'timeout' => '50000'
            ]
        ],
        'condensed' => true,
        'responsive' => true,
        'hover' => true,
        'showPageSummary' => false,
        'panel' => [
            'type' => \kartik\grid\GridView::TYPE_PRIMARY,
            'heading' => $name,
            'before' => '',
            'after' => false,
            'showFooter' => false
        ],
        'persistResize' => false,
        'toggleDataOptions' => ['minCount' => 1],
        'itemLabelSingle' => $name,
        'itemLabelPlural' => $name
    ]) ?>
</div>
