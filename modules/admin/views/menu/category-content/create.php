<?php

use app\models\Content;
use yii\helpers\Html;

/* @var $this yii\web\View */
$this->title = \app\models\Menu::getMenuByGet()->name;
$this->params['breadcrumbs'][] = ['label' => 'Calories', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="calories-create">
    <?= $this->render('forms/' . Content::getRender($menu->design_id), [
        'model' => $model,
        'menu' => $menu,
    ]) ?>
</div>
