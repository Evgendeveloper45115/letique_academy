<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Calories */

$this->title = 'Update';
$this->params['breadcrumbs'][] = ['label' => 'Update', 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="calories-update">
    <?= $this->render('forms/' . \app\models\Content::getRender($menu->design_id), [
        'model' => $model,
        'menu' => $menu,
    ]) ?>

</div>
