<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Content */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="calories-form">

    <?php $form = ActiveForm::begin([
        'options' => ['enctype' => 'multipart/form-data']
    ]); ?>

    <?= $form->field($model, 'cat')->hiddenInput(['value' => $menu->id, 'maxlength' => true])->label(false) ?>
    Это статичный блок
    <?php ActiveForm::end(); ?>

</div>
