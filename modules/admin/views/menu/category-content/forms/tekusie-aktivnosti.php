<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Content */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="calories-form">

    <?php $form = ActiveForm::begin([
        'options' => ['enctype' => 'multipart/form-data']
    ]); ?>

    <?= $form->field($model, 'cat')->hiddenInput(['value' => $menu->id, 'maxlength' => true])->label(false) ?>
    <?= $form->field($model, 'name')->textInput(['maxlength' => true])->label('Навание активности') ?>

    <?php
    echo '<label>До какой даты</label>';
    echo \kartik\date\DatePicker::widget([
        'model' => $model,
        'attribute' => 'date_to',
        'options' => ['placeholder' => 'Выбрать дату окончания'],
        //  'convertFormat' => true,
        'pluginOptions' => [
            'todayHighlight' => true,
            'autoclose' => true,
            'format' => 'yyyy-mm-dd',
        ]
    ]);
    ?>

    <?= $form->field($model, 'location')->textInput(['maxlength' => true]) ?>

    <label for="description">Описание</label>
    <?= $form->field($model, 'description', [
        'template' => "<div>{input}\n<div class=\"error\">{error}</div></div>",
    ])->widget(\zxbodya\yii2\tinymce\TinyMce::className(), [
        'options' => ['rows' => 15],
        'language' => 'ru',
//        'spellcheckerUrl'=>'http://speller.yandex.net/services/tinyspell',
        'fileManager' => [
            'class' => \zxbodya\yii2\elfinder\TinyMceElFinder::className(),
            'connectorRoute' => 'el-finder/connector',
        ],
    ]); ?>
    Аватар
    <?= \kartik\file\FileInput::widget([
        'model' => $model,
        'language' => 'ru',
        'attribute' => 'img2',
        'options' => [
            'multiple' => false
        ],

        'pluginOptions' => [
            'initialPreview' => $model->image ? '/uploads/' . $_GET['category'] . '/' . $model->image : false,
            'initialPreviewConfig' => [[
                'key' => $model->id
            ]],
            'initialPreviewAsData' => true,
            'deleteUrl' => \yii\helpers\Url::toRoute(['delete-image-avatar']),
            'overwriteInitial' => false,
        ]]) ?>
    Файлы(название файла: Это его title)
    <?= \kartik\file\FileInput::widget([
        'model' => $model,
        'language' => 'ru',
        'attribute' => 'img',
        'options' => [
            'multiple' => true
        ],

        'pluginOptions' => [
            'showPreview' => true,
            'showCaption' => true,
            'showRemove' => true,
            'showUpload' => true,
            'initialPreviewConfig' => $model->getImagesNames()['id'],
            'initialPreview' => $model->getImagesNames()['link'],
            //  'initialCaption' => "The Moon and the Earth",
            'initialPreviewAsData' => true,
            'deleteUrl' => \yii\helpers\Url::toRoute(['delete-image']),
            'overwriteInitial' => false,
        ]]) ?>


    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
