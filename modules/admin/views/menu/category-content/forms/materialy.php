<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Content */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="calories-form">

    <?php $form = ActiveForm::begin([
        'options' => ['enctype' => 'multipart/form-data']
    ]); ?>

    <?= $form->field($model, 'cat')->hiddenInput(['value' => $menu->id, 'maxlength' => true])->label(false) ?>
    <?= $form->field($model, 'name')->textInput(['maxlength' => true])->label('Название документа') ?>

    Аватар
    <?= \kartik\file\FileInput::widget([
        'model' => $model,
        'language' => 'ru',
        'attribute' => 'img2',
        'options' => [
            'multiple' => false
        ],

        'pluginOptions' => [
            'initialPreview' => $model->image ? '/uploads/' . $_GET['category'] . '/' . $model->image : false,
            'initialPreviewConfig' => [[
                'key' => $model->id
            ]],
            'initialPreviewAsData' => true,
            'deleteUrl' => \yii\helpers\Url::toRoute(['delete-image-avatar']),
            'overwriteInitial' => false,
        ]]) ?>
    Файлы
    <?= \kartik\file\FileInput::widget([
        'model' => $model,
        'language' => 'ru',
        'attribute' => 'img',
        'options' => [
            'multiple' => false
        ],

        'pluginOptions' => [
            'showPreview' => true,
            'showCaption' => true,
            'showRemove' => true,
            'showUpload' => true,
            'initialPreviewConfig' => $model->getImagesNames()['id'],
            'initialPreview' => $model->getImagesNames()['link'],
            //  'initialCaption' => "The Moon and the Earth",
            'initialPreviewAsData' => true,
            'deleteUrl' => \yii\helpers\Url::toRoute(['delete-image']),
            'overwriteInitial' => false,
        ]]) ?>


    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
