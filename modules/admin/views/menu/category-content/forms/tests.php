<?php
/**
 * @var $menu \app\models\Menu
 */

use kartik\form\ActiveForm;
use yii\helpers\Html;

?>
<div class="test-question-form">

    <?php $form = ActiveForm::begin([
        'fieldConfig' => [
            'options' => ['class' => 'form_group clearfix', 'enctype' => 'multipart/form-data',],
            'template' => "{label}\n<div class=\"col_right\">{input}\n<div class=\"error\">{error}</div></div>",
        ]
    ]); ?>

    <?= $form->field($model, 'test_id')->hiddenInput(['value' => $menu->id])->label(false) ?>
    <?= $form->field($model, 'title')->textInput() ?>
    <?= $form->field($model, 'test_one')->textInput() ?>
    <?= $form->field($model, 'test_two')->textInput() ?>
    <?= $form->field($model, 'test_three')->textInput() ?>
    <?= $form->field($model, 'test_four')->textInput() ?>
    <?= $form->field($model, 'test_five')->textInput() ?>
    <?= $form->field($model, 'description')->textInput() ?>
    <?= $form->field($model, 'is_text')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Сохранить' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
