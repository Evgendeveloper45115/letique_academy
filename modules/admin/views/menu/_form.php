<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use \kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\Menu */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="menu-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'points')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'design_id')->dropDownList(\app\models\Content::$design_ids) ?>
    <?= $form->field($model, 'is_news')->checkbox() ?>
    <label>Роль в какой будет виден раздел</label>
    <?php
    echo Select2::widget([
        'name' => 'roles',
        'value' => \yii\helpers\ArrayHelper::map($model->contentRoles, 'id', 'role'),
        'data' => \app\models\User::$roles,
        'theme' => Select2::THEME_DEFAULT,
        'options' => ['placeholder' => 'Выбрать роль ...', 'multiple' => true, 'autocomplete' => 'off'],
        'pluginOptions' => [
            'allowClear' => false
        ],
    ]);
    ?>
    <div class="form-group" style="margin-top:20px">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>
    <?php ActiveForm::end(); ?>

</div>
