<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\User */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = \app\models\Content::$cats[$_GET['cat_child']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <?= \kartik\grid\GridView::widget([
        'id' => 'kv-grid-demo',
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'email',
                'format' => 'raw',
                'value' => function (\app\models\User $model) {

                    return $model->email;
                },
                'filterInputOptions' => [
                    'class' => 'form-control',
                    'placeholder' => 'Email'
                ],

            ],
            [
                'attribute' => 'password',
                'label' => 'password',
                'value' => function (\app\models\User $model) {
                    return $model->password;
                }
            ],
            [
                'attribute' => 'role',
                'value' => function (\app\models\User $model) {
                    return \app\models\User::$roles[$model->role];
                },
                'filter' => \app\models\User::$roles
            ],
            [
                'attribute' => 'first_name',
                'filterInputOptions' => [
                    'class' => 'form-control',
                    'placeholder' => 'Имя'
                ],

            ],
            [
                'attribute' => 'last_name',
                'filterInputOptions' => [
                    'class' => 'form-control',
                    'placeholder' => 'Фамилия'
                ],

            ],
            [
                'attribute' => 'date_registration',
                'filterType' => \kartik\grid\GridView::FILTER_DATETIME,
                'filterWidgetOptions' => [
                    'pluginOptions' => [
                        //  'format' => 'dd-mm-yyyy',
                        'autoclose' => true,
                        'todayHighlight' => true,
                    ]
                ],
                'value' => function (\app\models\User $model) {
                    return $model->date_registration;
                }
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '<span style="text-wrap: none">{update} {delete}</span>',
                'header' => 'Действия',
                'buttons' => [
                    'update' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-pencil custom"></span>', \yii\helpers\Url::to(['update', 'id' => $model->id]), [
                            'title' => 'Редактировать'
                        ]);

                    },
                    'delete' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', \yii\helpers\Url::to(['delete', 'id' => $model->id]), [
                            'title' => 'Удалить',
                            'data' => [
                                'confirm' => 'Вы точно хотите удалить клиента?',
                                'method' => 'post',
                            ],
                        ]);

                    },
                ]
            ]
            ,
        ], // check the configuration for grid columns by clicking button above
        'containerOptions' => ['style' => 'overflow: auto'], // only set when $responsive = false
        'headerRowOptions' => ['class' => 'kartik-sheet-style'],
        'filterRowOptions' => ['class' => 'kartik-sheet-style'],
        'pjax' => true, // pjax is set to always true for this demo
        // set your toolbar
        /*'toolbar' => [
            [
                'content' => '<button type="button" class="btn btn-default generate_in_email" data-toggle="popover" data-placement="bottom" title="Выслать анкету участинку">Выслать анкету на участие</button>',
                'options' => ['class' => 'btn-group mr-2', 'id' => 'pModal']
            ],
            '{export}',
        ],*/
        'toggleDataContainer' => ['class' => 'btn-group mr-2'],
        // set export properties
        'export' => [
            'fontAwesome' => true
        ],
        // parameters from the demo form
        'bordered' => true,
        'striped' => true,
        'pjaxSettings' => [
            'options' => [
                'timeout' => '50000'
            ]
        ],
        'condensed' => true,
        'responsive' => true,
        'hover' => true,
        'showPageSummary' => false,
        'panel' => [
            'type' => \kartik\grid\GridView::TYPE_PRIMARY,
            'heading' => \app\models\Content::$cats[$_GET['cat_child']],
            'before' => '',
            'after' => false,
            'showFooter' => false
        ],
        'persistResize' => false,
        'toggleDataOptions' => ['minCount' => 1],
        'itemLabelSingle' => 'Бренд',
        'itemLabelPlural' => 'Брендов'
    ]) ?>
</div>
