<?php

use \app\models\Content;
use app\models\Menu;

$email = Yii::$app->user->getIdentity()->email;
$data = Menu::getItemsMenuAdmin();
?>
<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?= $directoryAsset ?>/img/user2-160x160.jpg" class="img-circle" alt="User Image"/>
            </div>
            <div class="pull-left info">
                <p><?= $email ?></p>

                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget' => 'tree'],
                'items' => [
                    ['label' => Yii::$app->user->identity->email, 'options' => ['class' => 'header pulse']],
                    ['label' => 'Новые участники', 'url' => ['user/index'], 'icon' => 'users', 'active' => Yii::$app->controller->id == 'user' ? true : false],
                    ['label' => 'Создать меню', 'url' => ['menu/index'], 'icon' => 'users', 'active' => Yii::$app->controller->id == 'menu' && !stristr(Yii::$app->controller->action->id, 'category') ? true : false],

                ],
            ]
        ) ?>
        <?= \startpl\yii2NestedSetsMenu\Menu::widget(
            [
                'items' => \startpl\yii2NestedSetsMenu\services\MenuArray::getData($data), // $data is your models|rows
                'options' => ['class' => 'sidebar-menu tree', 'data-widget' => 'tree'],
                'encodeLabels' => false,
                'activateParents' => true,
                'activeCssClass' => 'active',
            ]
        ) ?>

    </section>

</aside>
