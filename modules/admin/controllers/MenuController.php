<?php

namespace app\modules\admin\controllers;

use \app\models\search\TestQuestionSearch;
use app\models\Calories;
use app\models\Content;
use app\models\ContentHasImage;
use app\models\MenuHasUser;
use app\models\search\ContentSeacrch;
use app\models\search\UserSearch;
use app\models\TestQuestion;
use creocoder\nestedsets\NestedSetsBehavior;
use Yii;
use app\models\Menu;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * MenuController implements the CRUD actions for Menu model.
 */
class MenuController extends Controller
{

    public function actions()
    {
        return [
            'nodeMove' => [
                'class' => 'klisl\nestable\NodeMoveAction',
                'modelName' => Menu::class,
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Menu models.
     * @return mixed
     */
    public function actionIndex()
    {
        $root = Menu::find()->where(['depth' => '0']);
        return $this->render('index', [
            'query' => $root,
        ]);
    }

    /**
     * Displays a single Menu model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Menu model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        /** @var  $model Menu|NestedSetsBehavior */
        $modelDup = Menu::find()->where(['LIKE', 'name', '%' . $_POST['Menu']['name'] . '%', false])->all();
        $model = new Menu ();
        //Поиск корневого элемента
        $root = $model->find()->where(['depth' => '0'])->one();
        if ($model->load(Yii::$app->request->post())) {
            if (!empty($modelDup)) {
                $model->name = $model->name . '-' . count($modelDup);
            }
            //Если нет корневого элемента (пустая таблица)
            $model->appendTo($root); //вставляем в конец корневого элемента
            if ($model->save()) {
                if (isset($_POST['roles']) && !empty($_POST['roles'])) {
                    foreach ($_POST['roles'] as $role) {
                        $MHU = new MenuHasUser();
                        $MHU->role = $role;
                        $MHU->menu_id = $model->id;
                        $MHU->save(false);
                    }
                }
                return $this->redirect('index');
            }
        }

        return $this->render('create', [
            'model' => $model,
            'root' => $root
        ]);
    }

    /**
     * Updates an existing Menu model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            MenuHasUser::deleteAll(['menu_id' => $model->id]);
            foreach ($_POST['roles'] as $role) {
                $mhu = new MenuHasUser();
                $mhu->menu_id = $model->id;
                $mhu->role = $role;
                $mhu->save(false);
            }
            return $this->redirect(['index']);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Menu model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Menu model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Menu the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Menu::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionCategory()
    {
        /**
         * @var $model Menu
         */
        $model = Content::menuSearch();
        if ($model->design_id == 7) {
            $searchModel = new TestQuestionSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            return $this->render('tests/index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'model' => $model
            ]);
        }
        $searchModel = new ContentSeacrch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('category-content/category', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreateCategoryContent()
    {
        $menu = Content::menuSearch();
        $model = new Content();
        if ($menu->design_id == 7) {
            $model = new TestQuestion();
        }

        if ($model->load(Yii::$app->request->post()) && $model->save() && $this->saveFile($model)) {
            Yii::$app->session->setFlash('success', 'Успешно!');
            return $this->redirect(Yii::$app->request->referrer);
        }
        return $this->render('category-content/create', [
            'model' => $model,
            'menu' => $menu,
        ]);

    }

    public function actionUpdateCategoryContent()
    {

        $menu = Content::menuSearch();
        if ($menu->design_id == 7) {
            $model = TestQuestion::findOne($_GET['id']);
        } else {
            $model = Content::findOne($_GET['id']);
        }
        if ($model->load(Yii::$app->request->post()) && $this->saveFile($model) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Успешно!');
            return $this->redirect(Yii::$app->request->referrer);
        }
        return $this->render('category-content/update', [
            'model' => $model,
            'menu' => $menu,
        ]);

    }

    public function actionDeleteCategoryContent()
    {
        $model = Content::findOne(['id' => $_GET['id']]);
        if ($model->delete()) {
            Yii::$app->session->setFlash('success', 'Успешно!');
            return $this->redirect(Yii::$app->request->referrer);
        }
    }

    public function saveFile($model)
    {

        $menu = Content::menuSearch();

        $dir = Yii::getAlias('@webroot/uploads/' . $menu->id . '/');
        if (!file_exists($dir)) {
            mkdir($dir, 0777, true);
        }
        $files = UploadedFile::getInstances($model, 'img');
        $file_avatar = UploadedFile::getInstance($model, 'img2');
        if (!empty($files)) {
            foreach ($files as $file) {
                $withoutExt = preg_replace('/\\.[^.\\s]{3,4}$/', '', $file->name);
                $imgModel = new ContentHasImage();
                $imgModel->content_id = $model->id;
                $imgModel->title = $withoutExt;
                $fname = uniqid("", true) . '.' . $file->extension;
                $imgModel->name = $fname;
                $imgModel->save(false);
                $file->saveAs($dir . $fname);
            }
        }
        if ($file_avatar) {
            $fname = uniqid("", true) . '.' . $file_avatar->extension;
            $model->image = $fname;
            $model->save(false);
            $file_avatar->saveAs($dir . $fname);
        }

        return true;
    }

    public function actionDeleteImage()
    {
        $model = ContentHasImage::findOne($_POST['key']);
        $dir = Yii::getAlias('@webroot/uploads/' . $_GET['category'] . '/');
        unlink($dir . $model->name);
        $model->name = null;
        return json_encode($model->delete());
    }

    public function actionDeleteImageAvatar()
    {
        $model = Content::findOne($_POST['key']);
        $dir = Yii::getAlias('@webroot/uploads/' . $_GET['category'] . '/');
        unlink($dir . $model->image);
        $model->image = null;
        return json_encode($model->save(false));
    }

}
